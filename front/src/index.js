import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

import CookPage from './pages/cook';
import AdminPage from './pages/admin';
import ManagerPage from './pages/manager';
import LoginPage from './pages/login';
import StartPage from './pages/start';

import './index.css';

function App() {
  return (
    <div className = "mainApp">
        <Router>
          <Switch>
            <Route path="/admin" component={AdminPage}/>
            <Route path="/cook" component={CookPage}/>
            <Route path="/manager" component={ManagerPage}/>
            <Route exact path="/login" component={LoginPage}/>
            <Route path="/" component={StartPage}/>
          </Switch>
        </Router>
    </div>
  );
}

const rootElement = document.getElementById('root');
ReactDOM.render(<App/>, rootElement);
