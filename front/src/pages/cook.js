import React from 'react';
import Cookies from 'universal-cookie';
import axios from 'axios';

import LoginCheck from '../components/LoginCheck';
import Header from '../components/Header';

import {Button, Col, Grid, Row, Table} from 'react-bootstrap';

class CookPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            cooking: undefined,
            waiting: undefined,
        };

        const cookies = new Cookies();

        axios.defaults.headers.common['Authorization'] = cookies.get('token');

        this.refuseCooking = this.refuseCooking.bind(this);
        this.startWaiting = this.startWaiting.bind(this);
        this.refuseStartCooking = this.refuseStartCooking.bind(this);
    }

    componentDidMount() {
        this.updateState.bind(this)();
    }

    componentWillUnmount() {
        clearInterval(this.findOrderInterval);
    }

    updateState() {
        axios.get('http://localhost:8080/cook/state')
            .then(
                (res) => {
                    if (!res.data.cooking && res.data.waiting) {
                        this.findOrderInterval = setInterval(this.findOrder.bind(this), 1000);
                    }

                    this.setState({
                        cooking: res.data.cooking,
                        waiting: res.data.waiting,
                        dish: res.data.dish
                    });
                },
                (error) => {
                    // alert(error)
                })
    }

    findOrder() {
        axios.get('http://localhost:8080/cook/order')
            .then(
                (res) => {
                    if (res.data.dish) {

                        this.setState({
                            dish: res.data.dish,
                            cooking: res.data.cooking,
                            waiting: res.data.waiting,
                        });
                        clearInterval(this.findOrderInterval);
                    }
                },
                (error) => {
                    alert(error)
                })
    }

    startWaiting() {
        axios.put('http://localhost:8080/cook/waiting')
            .then(
                (res) => {

                    this.setState({
                        cooking: res.data.cooking,
                        waiting: res.data.waiting
                    });

                    this.findOrderInterval = setInterval(this.findOrder.bind(this), 1000);
                },
                (error) => {
                    alert(error);
                });
    }

    stopWaiting() {
        axios.post('http://localhost:8080/cook/stopwainting')
            .then(
                (res) => {
                    this.setState({
                        cooking: res.data.cooking,
                        waiting: res.data.waiting,
                        dish: res.data.dish
                    });
                    clearInterval(this.findOrderInterval);
                },
                (error) => {
                    alert(error);
                });
    }

    refuseStartCooking() {
        axios.post('http://localhost:8080/cook/refusechoosing')
            .then(
                (res) => {
                    this.setState({
                        cooking: res.data.cooking,
                        waiting: res.data.waiting,
                        dish: res.data.dish
                    });
                },
                (error) => {
                    alert(error);
                });
    }

    startCooking() {
        axios.put('http://localhost:8080/cook/cooking')
            .then(
                (res) => {

                    this.setState({
                        cooking: res.data.cooking,
                        waiting: res.data.waiting,
                        dish: res.data.dish
                    });
                    clearInterval(this.findOrderInterval);
                },
                (error) => {
                    alert(error);
                });
    }

    endCooking() {
        axios.put('http://localhost:8080/cook/end')
            .then(
                (res) => {

                    this.setState({
                        cooking: res.data.cooking,
                        waiting: res.data.waiting,
                        dish: res.data.dish
                    });
                },
                (error) => {
                    alert(error);
                });
    }

    refuseCooking() {
        axios.post('http://localhost:8080/cook/refusecooking')
            .then(
                (res) => {
                    this.setState({
                        cooking: res.data.cooking,
                        waiting: res.data.waiting,
                        dish: res.data.dish
                    });
                },
                (error) => {
                    alert(error);
                });
    }

    render() {
        const cookies = new Cookies();

        let realName = cookies.get('realName');

        let content;

        if (this.state.waiting !== undefined && this.state.cooking !== undefined) {

            if (!this.state.waiting && !this.state.cooking) {
                content = <PauseInfoBlock
                    startWaiting={this.startWaiting}
                >
                </PauseInfoBlock>;
            }

            if (this.state.waiting && !this.state.cooking) {
                content = <WaitDishInfoBlock
                    stopWaiting={this.stopWaiting.bind(this)}
                >

                </WaitDishInfoBlock>
            }

            if (this.state.waiting && this.state.cooking) {
                content = <div>
                    <NewDishInfoBlock
                        dishName={this.state.dish.name}
                        minCookTime={this.state.dish.minCookTime}
                        maxCookTime={this.state.dish.maxCookTime}
                        startCooking={this.startCooking.bind(this)}
                        refuseStartCooking={this.refuseStartCooking}
                    >
                    </NewDishInfoBlock>
                    <IngredientsTable
                        ingredients={this.state.dish.ingredients}
                    >
                    </IngredientsTable>
                </div>;
            }

            if (!this.state.waiting && this.state.cooking) {
                content =
                    <div>
                        <InfoBlock
                            dishStartCookTime={this.state.dish.startCookTime}
                            dishMaxCookTime={this.state.dish.maxCookTime}
                            minCookTime={this.state.dish.minCookTime}
                            dishName={this.state.dish.name}
                            endCooking={this.endCooking.bind(this)}
                            refuseCooking={this.refuseCooking}
                            startCookTime={this.state.dish.startCookTime}
                        >
                        </InfoBlock>
                        <IngredientsTable
                            ingredients={this.state.dish.ingredients}
                        >
                        </IngredientsTable>
                    </div>;
            }
        }
        return (<LoginCheck
            userRole={'COOK'}
            elem={
                <div>
                    <Header
                        text={'Повар ' + realName}>
                    </Header>
                    {content}
                </div>
            }/>);
    }
}

class IngredientsTable extends React.Component {

    render() {
        let rows = this.props.ingredients.map(
            (ingredient, rowId) => {
                return <IngredientRow
                    key={rowId}
                    rowId={rowId}
                    data={ingredient}
                />
            }
        );

        return (
            <Grid>
                <div className="infoblock">
                    <div className="infoblockname">Ингредиенты</div>
                    <div>
                        <Table striped bordered condensed hover>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Ингредиент</th>
                                <th>Количество</th>
                            </tr>
                            </thead>
                            <tbody>
                            {rows}
                            </tbody>
                        </Table>
                    </div>
                </div>

            </Grid>
        );
    }
}

const IngredientRow = (props) => {
    return (
        <tr>
            <td>{props.rowId + 1}</td>
            <td>{props.data.synonyms[0].name}</td>
            <td>{props.data.synonyms[0].count}</td>
        </tr>
    );
};

class InfoBlock extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};

        let options = {
            hour: 'numeric',
            minute: 'numeric',
            second: 'numeric'
        };

        this.calcRemaningTime = this.calcRemaningTime.bind(this);
        this.tick = this.tick.bind(this);

        this.state.remainingCookTime = this.calcRemaningTime();
        this.state.startTime = new Date(this.props.startCookTime).toLocaleString("ru", options);
    }

    componentDidMount() {
        this.timer = setInterval(this.tick, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    calcRemaningTime() {
        let sec = (this.props.dishStartCookTime + this.props.dishMaxCookTime * 1000 - new Date().getTime()) / 1000;
        if (sec < 0) {
            return "00:00:00";
        } else {
            let hour = Math.floor(sec / 3600) > 9 ? Math.floor(sec / 3600) : '0' + Math.floor(sec / 3600);
            let min = Math.floor((sec % 3600) / 60) > 9 ? Math.floor((sec % 3600) / 60) : '0' + Math.floor((sec % 3600) / 60);
            let s = Math.floor(sec % 60) > 9 ? Math.floor(sec % 60) : '0' + Math.floor(sec % 60);
            let strDate = hour + ':' + min + ':' + s;
            return strDate;
        }
    }

    goodTime(sec) {
        if (sec < 0) {
            return "00:00:00";
        } else {
            sec = sec % (60 * 60 * 24) + 60 * 60 * 3;
            let hour = (Math.floor(sec / 3600) > 9 ? Math.floor(sec / 3600) : '0' + Math.floor(sec / 3600));
            let min = Math.floor((sec % 3600) / 60) > 9 ? Math.floor((sec % 3600) / 60) : '0' + Math.floor((sec % 3600) / 60);
            let s = Math.floor(sec % 60) > 9 ? Math.floor(sec % 60) : '0' + Math.floor(sec % 60);
            let strDate = hour + ':' + min + ':' + s;
            return strDate;
        }
    }

    tick() {
        let time = this.calcRemaningTime();

        this.setState({remainingCookTime: time});
    }

    render() {
        return (
            <Grid>
                <div className="infoblock">
                    <div className="infoblockname"> Текущий заказ</div>
                    <Row className="show-grid">
                        <Col md={4}>
                            <div>
                                <p>Блюдо:</p> <p>{this.props.dishName}</p>
                            </div>
                        </Col>
                        <Col md={2}>
                            <div className="textleft">
                                <p>Начало:</p> <p>{this.goodTime(this.props.startCookTime / 1000)}</p>
                            </div>
                        </Col>
                        <Col md={2}>
                            <div className="textleft">
                                <p>Осталось:</p> <p>{this.state.remainingCookTime}</p>
                            </div>
                        </Col>
                        <Col md={2}>
                            <div className="textright">
                                <Button style={{fontSize: 28}}
                                        bsStyle="success"
                                        onClick={this.props.endCooking}
                                >
                                    Завершение
                                </Button>
                            </div>
                        </Col>
                        <Col md={2}>
                            <div className="textright">
                                <Button style={{fontSize: 28}}
                                        bsStyle="danger"
                                        onClick={this.props.refuseCooking}
                                >
                                    Отказ
                                </Button>
                            </div>
                        </Col>
                    </Row>
                </div>
            </Grid>
        );
    }
}

class NewDishInfoBlock extends React.Component {
    render() {

        let sec = this.props.minCookTime;

        let hour = Math.floor(sec / 3600) > 9
            ? Math.floor(sec / 3600)
            : '0' + Math.floor(sec / 3600);
        let min = Math.floor((sec % 3600) / 60) > 9
            ? Math.floor((sec % 3600) / 60)
            : '0' + Math.floor((sec % 3600) / 60);
        let s = Math.floor(sec % 60) > 9
            ? Math.floor(sec % 60)
            : '0' + Math.floor(sec % 60);

        let avgTime = hour + ':' + min + ':' + s + " - ";

        sec = this.props.maxCookTime;

        hour = Math.floor(sec / 3600) > 9
            ? Math.floor(sec / 3600)
            : '0' + Math.floor(sec / 3600);
        min = Math.floor((sec % 3600) / 60) > 9
            ? Math.floor((sec % 3600) / 60)
            : '0' + Math.floor((sec % 3600) / 60);
        s = Math.floor(sec % 60) > 9
            ? Math.floor(sec % 60)
            : '0' + Math.floor(sec % 60);

        avgTime += hour + ':' + min + ':' + s;

        return (
            <Grid>
                <div className="infoblock">
                    <div className="infoblockname"> Новый заказ</div>
                    <Row className="show-grid">
                        <Col md={4}>
                            <div>
                                <p>Блюдо:</p> <p>{this.props.dishName}</p>
                            </div>
                        </Col>
                        <Col md={3}>
                            <div className="textleft">
                                <p>Время приготовления:</p> <p>{avgTime}</p>
                            </div>
                        </Col>
                        <Col mdOffset={1} md={2}>
                            <div className="textright">
                                <Button style={{fontSize: 28}}
                                        bsStyle="success"
                                        onClick={this.props.startCooking}
                                >
                                    Принять
                                </Button>
                            </div>
                        </Col>
                        <Col md={2}>
                            <div className="textright">
                                <Button style={{fontSize: 28}}
                                        bsStyle="danger"
                                        onClick={this.props.refuseStartCooking}
                                >
                                    Отклонить
                                </Button>
                            </div>
                        </Col>
                    </Row>
                </div>
            </Grid>
        );
    }
}

class WaitDishInfoBlock extends React.Component {
    render() {
        return (
            <Grid>
                <div className="infoblock textcenter">
                    <div className="infoblockname"> Поиск нового заказа</div>
                    <div>
                        <p> Сиситема находится в поисках подходящего заказа </p>
                        <p> Точите ножи и мойте ручки :) </p>
                    </div>
                    <div className="stopwaitingbtn">
                        <Button style={{fontSize: 28}}
                                bsStyle="danger"
                                onClick={
                                    () => {
                                        this.props.stopWaiting();
                                    }
                                }
                        >
                            Отменить поиск
                        </Button>
                    </div>
                </div>
            </Grid>
        );
    }
}

class PauseInfoBlock extends React.Component {
    render() {
        return (
            <Grid>
                <div className="infoblock">
                    <div className="infoblockname"> Начать поиск новго заказа?</div>
                    <div className="textcenter">
                        <Button style={{fontSize: 28}}
                                bsStyle="success"
                                onClick={this.props.startWaiting}
                        >
                            Начать
                        </Button>
                    </div>
                </div>
            </Grid>
        );
    }
}

export default CookPage;
