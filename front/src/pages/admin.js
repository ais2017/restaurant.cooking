import React from 'react';
import axios from 'axios';

import Cookies from 'universal-cookie';
import Header from '../components/Header';

import {
    Button,
    Checkbox,
    Col,
    Collapse,
    ControlLabel,
    Form,
    FormControl,
    FormGroup,
    Grid,
    Row,
    Table
} from 'react-bootstrap';
import LoginCheck from "../components/LoginCheck";

class AdminPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            selectedUser: undefined
        };
        const cookies = new Cookies();

        axios.defaults.headers.common['Authorization'] = cookies.get('token');

        this.submitChangeUser = this.submitChangeUser.bind(this);
        this.editUser = this.editUser.bind(this);
        this.deleteUser = this.deleteUser.bind(this);
        this.addUser = this.addUser.bind(this);
    }

    componentDidMount() {
        this.updateState.bind(this)();
    }

    updateState() {
        axios.get('http://localhost:8080/admin/user')
            .then(
                (res) => {

                    this.setState({
                        users: res.data.users,
                        specialization: res.data.specialization,
                        defSpecialization: res.data.defSpecialization
                    });
                },
                (error) => {
                    // alert(error)
                })
    }

    editUser(id) {
        for (let i = 0; i < this.state.users.length; i++) {
            if (this.state.users[i].id === id) {
                this.setState({selectedUser: i});
                return;
            }
        }
    }

    addUser() {
        this.setState({selectedUser: -1});
    }

    deleteUser(id, cookId) {

        axios.delete('http://localhost:8080/admin/user',
            {
                params:
                    {
                        userId: id,
                        cookId: cookId
                    }
            })
            .then(
                (res) => {
                    this.setState({
                        users: res.data.users,
                        specialization: res.data.specialization,
                        defSpecialization: res.data.defSpecialization
                    });
                },
                (error) => {
                    alert(error)
                })
    }

    submitChangeUser(user) {
        axios.put('http://localhost:8080/admin/user',
            {
                id: user.id,
                userName: user.userName,
                realName: user.realName,
                role: user.role,
                changePassword: user.changePassword,
                password: user.password,
                specialization: user.specialization,
                cookId: user.cookId
            })
            .then(
                (res) => {


                    let cookies = new Cookies();

                    if (this.state.selectedUser !== -1 && this.state.users[this.state.selectedUser].userName === cookies.get('username')) {

                        cookies.remove('username');
                        cookies.remove('realName');
                        cookies.remove('role');
                        cookies.remove('token');
                    }

                    this.setState({
                        users: res.data.users,
                        defSpecialization: res.data.defSpecialization
                    });
                },
                (error) => {
                    alert(error)
                })
    }

    render() {

        const cookies = new Cookies();

        let realName = cookies.get('realName');

        let content = '';
        if (this.state.selectedUser != null) {
            content =
                <EditUserPlate
                    defSpecialization={this.state.defSpecialization}
                    user={this.state.users[this.state.selectedUser]}
                    submitChangeUser={this.submitChangeUser}
                    selectedUser={this.state.selectedUser}
                >
                </EditUserPlate>;
        }
        let choseUserPlateContent = '';
        if (this.state.users !== undefined) {
            choseUserPlateContent = <ChoseUserPlate
                users={this.state.users}
                editUser={this.editUser}
                deleteUser={this.deleteUser}
                addUser={this.addUser}
            >
            </ChoseUserPlate>;
        }

        return (
            <LoginCheck
                userRole={'ADMIN'}
                elem={
                    <div>
                        <Header
                            text={'Администратор ' + realName}>
                        </Header>
                        {choseUserPlateContent}
                        {content}
                    </div>
                }/>);
    }
}

class ChoseUserPlate extends React.Component {

    render() {
        return (
            <Grid>
                <div className="infoblock">
                    <div className="infoblockname">Пользователи</div>
                    <div>
                        <div>
                            <Row>
                                <Col mdOffset={1} md={10}>
                                    <div>
                                        <SelectUserTable
                                            users={this.props.users}
                                            editUser={this.props.editUser}
                                            deleteUser={this.props.deleteUser}
                                        >
                                        </SelectUserTable>
                                    </div>
                                    <Button style={{fontSize: 20}}
                                            onClick={() => {
                                                this.props.addUser()
                                            }}
                                    >
                                        Новый пользователь
                                    </Button>
                                </Col>
                            </Row>
                        </div>
                    </div>
                </div>

            </Grid>

        );
    }
}

class SelectUserTable extends React.Component {

    render() {

        let rows = this.props.users.map(
            (user, rowNumber) => {
                return <UserRow
                    key={user.id}
                    data={user}
                    rowNumber={rowNumber + 1}
                    editUser={this.props.editUser}
                    deleteUser={this.props.deleteUser}
                />
            }
        );

        return (
            <Table striped bordered condensed hover>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Имя пользователя</th>
                    <th>Полное имя</th>
                    <th>Права</th>
                </tr>
                </thead>
                <tbody>
                {rows}
                </tbody>
            </Table>
        );
    }
}

class UserRow extends React.Component {

    render() {
        let role;
        if (this.props.data.role === 'MANAGER') {
            role = 'Менеджер';
        } else if (this.props.data.role === 'ADMIN') {
            role = 'Администратор';
        } else if (this.props.data.role === 'COOK') {
            role = 'Повар';
        } else {
            role = 'Неизвестно';
        }

        let cookies = new Cookies();
        let deleteButton = '';

        if (!(cookies.get('username') === this.props.data.userName)) {
            deleteButton = <img
                className="binimage"
                alt=""
                src="/delete.png"
                onClick={() => {
                    this.props.deleteUser(this.props.data.id, this.props.data.cookId)
                }}
            />;
        }
        return (
            <tr>
                <td>{this.props.rowNumber}</td>
                <td>{this.props.data.userName}</td>
                <td>{this.props.data.realName}</td>
                <td>{role}</td>
                <td className="textcenter">
                    <img
                        className="binimage"
                        alt=""
                        src="/edit.jpg"
                        onClick={() => {
                            this.props.editUser(this.props.data.id)
                        }}
                    />
                </td>
                <td className="textcenter">
                    {deleteButton}
                </td>
            </tr>
        );
    }
};

class EditUserPlate extends React.Component {

    constructor(props) {
        super(props);


        this.state = this.takeStateFromProps(this.props);
    }

    takeStateFromProps(props) {
        if (props.user === undefined) {
            return {
                refresh: false,
                id: -1,
                userName: "",
                realName: "",
                role: "COOK",
                password: '',
                changePassword: false,
                specialization: this.props.defSpecialization,
                cookId: -1
            }
        }
        return {
            refresh: false,
            id: props.user.id,
            userName: props.user.userName,
            realName: props.user.realName,
            role: props.user.role,
            password: '',
            changePassword: false,
            specialization: props.user.specialization === undefined ? this.props.defSpecialization : props.user.specialization,
            cookId: props.user.cookId
        }
    }

    componentWillReceiveProps(props) {
        this.setState(this.takeStateFromProps(props));
    }

    updateSpecialization(index) {
        let copy = this.state.specialization.slice();
        copy[index].flag = !copy[index].flag;
        this.setState({specialization: copy})
    }

    render() {
        let cookeis = new Cookies();
        let yourUsername = cookeis.get('username');
        return (
            <Grid>
                <div className="infoblock">
                    <div className="infoblockname">Редактирование</div>
                    <div>
                        <Row>
                            <div className="textcenter">
                                <Form horizontal>
                                    <FormGroup>
                                        <Col className="rownameinfo" componentClass={ControlLabel} sm={3}>
                                            Пользователь
                                        </Col>
                                        <Col sm={8}>
                                            <FormControl
                                                placeholder="Пользователь"
                                                value={this.state.userName}
                                                onChange={
                                                    (event) => {
                                                        this.setState({userName: event.target.value})
                                                    }
                                                }
                                            />
                                        </Col>
                                    </FormGroup>

                                    <FormGroup>
                                        <Col className="rownameinfo" componentClass={ControlLabel} sm={3}>
                                            Полное имя
                                        </Col>
                                        <Col sm={8}>
                                            <FormControl
                                                placeholder="Полное имя"
                                                value={this.state.realName}
                                                onChange={(event) => {
                                                    this.setState({realName: event.target.value})
                                                }}/>
                                        </Col>
                                    </FormGroup>

                                    <FormGroup>
                                        <Col className="rownameinfo" componentClass={ControlLabel} sm={3}>
                                            Пароль
                                        </Col>
                                        <Col sm={5}>
                                            <FormControl
                                                type="password"
                                                placeholder="Пароль"
                                                value={this.state.password}
                                                onChange={(event) => {
                                                    this.setState({password: event.target.value})
                                                }}/>
                                        </Col>
                                        <Col className="textright" sm={3}>
                                            <Checkbox
                                                onChange={
                                                    () => {
                                                        this.setState({changePassword: !this.state.changePassword})
                                                    }
                                                }
                                                defaultChecked={this.state.changePassword}
                                            >
                                                Сохранить пароль</Checkbox>
                                        </Col>
                                    </FormGroup>
                                    <FormGroup>
                                        <Col className="rownameinfo" componentClass={ControlLabel} sm={3}>
                                            Права
                                        </Col>
                                        <Col sm={8}>
                                            {yourUsername === this.state.userName ?
                                                <FormControl disabled={true}
                                                    defaultValue={'Администратор'}
                                                >
                                                </FormControl>:
                                                <FormControl
                                                    onChange={() => {
                                                        this.setState({role: this.inputEl.value})
                                                    }}
                                                    inputRef={el => this.inputEl = el}
                                                    componentClass="select"
                                                    value={this.state.role}
                                                >

                                                    <option value="COOK">Повар</option>
                                                    <option value="MANAGER">Менеджер</option>
                                                    <option value="ADMIN">Администратор</option>
                                                </FormControl>}

                                        </Col>
                                    </FormGroup>
                                    <Collapse in={this.state.role === 'COOK'}>
                                        <FormGroup>
                                            <Col className="rownameinfo" componentClass={ControlLabel} sm={3}>
                                                Специализация
                                            </Col>
                                            <SpecialixationBoxes
                                                specialization={this.state.specialization}
                                                updateSpecialization={ this.updateSpecialization.bind(this)}
                                            >
                                            </SpecialixationBoxes>
                                        </FormGroup>
                                    </Collapse>
                                    <FormGroup className="textright">
                                        <Col smOffset={9} sm={2}>
                                            <Button
                                                bsStyle="success"
                                                onClick={
                                                    () => {
                                                        this.props.submitChangeUser({
                                                                id: this.state.id,
                                                                userName: this.state.userName,
                                                                realName: this.state.realName,
                                                                role: this.state.role,
                                                                password: this.state.password,
                                                                changePassword: this.state.changePassword,
                                                                specialization: this.state.specialization,
                                                                cookId: this.state.cookId
                                                            }
                                                        )
                                                    }
                                                }>
                                                Сохранить</Button>
                                        </Col>
                                    </FormGroup>
                                </Form>
                            </div>
                        </Row>

                    </div>
                </div>

            </Grid>

        );
    }
}

class SpecialixationBoxes extends React.Component {

    // constructor(props) {
    //     super(props);
    //
    //     this.state = this.stateFromProps(props);
    //
    // }
    //
    // stateFromProps(props) {
    //     return {
    //         specialization: props.specialization.slice()
    //     };
    // }
    //
    // componentWillReceiveProps(props) {
    //     this.setState(this.stateFromProps(props));
    // }

    render() {
        let boxes = this.props.specialization.map(
            (spec, rowNumber) => {
                return <Checkbox inline
                                 key = {rowNumber}
                                 onChange={
                                     () => {
                                         this.props.updateSpecialization(rowNumber);
                                     }
                                 }
                                 checked={spec.flag}
                >
                    {spec.name}
                </Checkbox>
            }
        );

        return <Col className="textleft" sm={8}>
            {boxes}
        </Col>
    }
};

export default AdminPage;
