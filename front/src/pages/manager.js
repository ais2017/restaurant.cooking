import React from 'react';

import axios from 'axios';

import Cookies from 'universal-cookie';
import Header from '../components/Header';

import {
    Row,
    ControlLabel,
    Button,
    Col,
    Form,
    FormControl,
    FormGroup,
    Grid,
    Table
} from 'react-bootstrap';
import LoginCheck from "../components/LoginCheck";

class ManagerPage extends React.Component {

    constructor(props) {
        super(props);

        const cookies = new Cookies();

        axios.defaults.headers.common['Authorization'] = cookies.get('token');

        this.state = {
            selectedDish: undefined
        };
    }

    componentDidMount() {
        this.updateState.bind(this)();
    }

    saveChanges(dish) {
        console.log('dish', dish)
        axios.put('http://localhost:8080/manager/dish',
            {
                dish: {
                    id: dish.id,
                    ingredients: dish.ingredients.slice(),
                    maxCookTime: dish.maxCookTime,
                    minCookTime: dish.minCookTime,
                    name: dish.name,
                    specialization: dish.specialization
                }
            }
        )
            .then(
                (res) => {

                    console.log('res.data', res.data);
                    this.setState({
                        dishes: res.data.dishes
                    });
                },
                (error) => {
                    alert(error)
                })
    }

    updateState() {
        axios.get('http://localhost:8080/manager/dish')
            .then(
                (res) => {

                    this.setState({
                        dishes: res.data.dishes
                    });
                },
                (error) => {
                    // alert(error)
                })
    }

    editDish(id) {
        for (let i = 0; i < this.state.dishes.length; i++) {
            if (this.state.dishes[i].id === id) {
                this.setState(
                    {
                        selectedDish: i,
                        selectedIngredient: undefined
                    });
                return;
            }
        }
    }

    addDish() {
        let copy = this.state.dishes.slice();
        copy.push({
            id: -1,
            ingredients: [],
            maxCookTime: '',
            minCookTime: '',
            name: '',
            specialization: 'MEAT'
        });
        this.setState({dishes: copy});
    }

    deleteDish(id) {
        let copy = [];
        let found = false;
        for (let i = 0; i < this.state.dishes.length; i++) {
            if (this.state.dishes[i].id === id) {
                found = true;
                continue;
            }
            copy.push(this.state.dishes[i]);
        }
        if (found) {
            this.setState({dishes: copy, selectedDish: undefined});
        }
    }

    render() {

        const cookies = new Cookies();

        let realName = cookies.get('realName');

        let choseDishPlate = '';
        if (this.state.dishes !== undefined) {
            choseDishPlate = <ChoseDishPlate
                dishes={this.state.dishes}
                editDish={this.editDish.bind(this)}
                deleteDish={this.deleteDish.bind(this)}
                addDish={this.addDish.bind(this)}
            >
            </ChoseDishPlate>
        }

        let editPlate = '';
        if (this.state.selectedDish != null) {
            editPlate =
                <EditPlate
                    saveChanges={
                        (dish) => {
                            this.saveChanges.bind(this)(dish);
                        }
                    }
                    dish={this.state.dishes[this.state.selectedDish]}
                >
                </EditPlate>
        }

        return (
            <LoginCheck
                userRole={'MANAGER'}
                elem={
                    <div>
                        <Header
                            text={'Менеджер ' + realName}>
                        </Header>
                        {choseDishPlate}
                        {editPlate}
                    </div>
                }/>);
    }
}

class EditPlate extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            selectedIngredient: undefined,
            dish: props.dish
        };

        this.saveChanges = this.saveChanges.bind(this);
    }

    saveChanges() {
        console.log(this.state);
    }

    componentWillReceiveProps(props) {
        this.setState({
            selectedIngredient: undefined,
            dish: props.dish
        });
    }

    editIngredient(id) {
        this.setState({selectedIngredient: id});
    }

    deleteIngredient(id) {
        let copy = {
            id: this.state.dish.id,
            ingredients: this.state.dish.ingredients.slice(),
            maxCookTime: this.state.dish.maxCookTime,
            minCookTime: this.state.dish.minCookTime,
            name: this.state.dish.name,
            specialization: this.state.dish.specialization
        };

        copy.ingredients.splice(id, 1);
        this.setState({dish: copy, selectedIngredient: undefined});
    }

    updateIngredient(ingredients) {
        let copy = {
            id: this.state.dish.id,
            ingredients: this.state.dish.ingredients.slice(),
            maxCookTime: this.state.dish.maxCookTime,
            minCookTime: this.state.dish.minCookTime,
            name: this.state.dish.name,
            specialization: this.state.dish.specialization
        };

        copy.ingredients[this.state.selectedIngredient].synonyms = ingredients;
        this.setState({dish: copy});
    }

    addDishIngredient() {
        let copy = {
            id: this.state.dish.id,
            ingredients: this.state.dish.ingredients.slice(),
            maxCookTime: this.state.dish.maxCookTime,
            minCookTime: this.state.dish.minCookTime,
            name: this.state.dish.name,
            specialization: this.state.dish.specialization
        };

        copy.ingredients.push({
            id: -1,
            synonyms: []
        });
        this.setState({dish: copy});
    }

    addIngredient() {
        let copy = {
            id: this.state.dish.id,
            ingredients: this.state.dish.ingredients.slice(),
            maxCookTime: this.state.dish.maxCookTime,
            minCookTime: this.state.dish.minCookTime,
            name: this.state.dish.name,
            specialization: this.state.dish.specialization
        };

        copy.ingredients[this.state.selectedIngredient].synonyms.push({
            id: -1,
            name: '',
            count: '',
        });
        this.setState({dish: copy});
    }

    render() {

        let ingredientPlate = '';

        if (this.state.selectedIngredient != null) {
            ingredientPlate =
                <EditIngredientPlate
                    updateIngredients={
                        (ingredients) => {
                            this.updateIngredient(ingredients);
                        }
                    }
                    ingredients={this.state.dish.ingredients[this.state.selectedIngredient].synonyms}
                    addIngredient={
                        () => {
                            this.addIngredient.bind(this)();
                        }
                    }
                >
                </EditIngredientPlate>
        }
        console.log(this.state.dish.name);
        return (
            <div>
                <EditDishPlate
                    saveChanges={
                        () => {
                            this.props.saveChanges(this.state.dish);
                        }
                    }
                    updateDish={
                        (dish) => {
                            this.setState({dish: dish});
                        }
                    }
                    dish={this.state.dish}
                >
                </EditDishPlate>
                <ChoseIngredientsPlate
                    editIngredient={this.editIngredient.bind(this)}
                    deleteIngredient={this.deleteIngredient.bind(this)}
                    ingredients={this.state.dish.ingredients}
                    addDishIngredient={
                        () => {
                            this.addDishIngredient.bind(this)();
                        }
                    }
                >
                </ChoseIngredientsPlate>
                {ingredientPlate}
            </div>
        );
    }
}

class ChoseDishPlate extends React.Component {

    render() {
        return (
            <Grid>

                <div className="infoblock">
                    <div className="infoblockname">Список блюд</div>
                    <Row>
                        <Col mdOffset={1} md={10}>
                            <div>
                                <div>
                                    <div>
                                        <SelectDishTable
                                            dishes={this.props.dishes}
                                            editDish={this.props.editDish}
                                            deleteDish={this.props.deleteDish}
                                            addDish={this.props.addDish}
                                        >
                                        </SelectDishTable>
                                        <Button style={{fontSize: 20}}
                                                onClick={() => {
                                                    this.props.addDish()
                                                }}
                                        >
                                            Новое блюдо
                                        </Button>
                                    </div>

                                </div>
                            </div>
                        </Col>
                    </Row>
                </div>

            </Grid>

        );
    }
}

class SelectDishTable extends React.Component {

    render() {

        let rows = this.props.dishes.map(
            (dish, rawId) => {
                return <DishRow
                    key={dish.id}
                    rawId={rawId + 1}
                    data={dish}
                    editDish={this.props.editDish}
                    deleteDish={this.props.deleteDish}
                    addDish={this.props.addDish}
                />
            }
        );

        return (
            <Table striped bordered condensed hover>
                <thead>
                <tr>
                    <th className="td_1">#</th>
                    <th className="td_2">Название</th>
                    <th>Специализация</th>
                </tr>
                </thead>
                <tbody>
                {rows}
                </tbody>
            </Table>
        );
    }
}

class DishRow extends React.Component {

    render() {
        let type;
        if (this.props.data.specialization === 'MEAT') {
            type = 'Мясо';
        } else if (this.props.data.specialization === 'FISH') {
            type = 'Рыба';
        } else if (this.props.data.specialization === 'DESSERT') {
            type = 'Выпечка';
        }

        return (
            <tr>
                <td className="td_1">{this.props.rawId}</td>
                <td>{this.props.data.name}</td>
                <td>{type}</td>
                <td className="td_btn">
                    <img
                        className="binimage"
                        alt=""
                        src="/edit.jpg"
                        onClick={() => {
                            this.props.editDish(this.props.data.id)
                        }}
                    />
                </td>
                <td className="td_btn">
                    <img
                        className="binimage"
                        alt=""
                        src="/delete.png"
                        onClick={() => {
                            this.props.deleteDish(this.props.data.id)
                        }}
                    />
                </td>
            </tr>
        );
    }
}

class EditDishPlate extends React.Component {

    render() {
        console.log(this.props.dish.name);
        return (
            <Grid>
                <div className="infoblock">
                    <div className="infoblockname">Информация о блюде</div>
                    <div>
                        <Row>
                            <div className="textcenter">
                                <Form horizontal>
                                    <FormGroup>
                                        <Col className="rownameinfo" componentClass={ControlLabel} sm={3}>
                                            Название
                                        </Col>
                                        <Col sm={8}>
                                            <FormControl
                                                placeholder="Название"
                                                value={this.props.dish.name}
                                                onChange={
                                                    (event) => {
                                                        this.props.dish.name = event.target.value;
                                                        this.props.updateDish(this.props.dish);
                                                    }
                                                }
                                            />
                                        </Col>
                                    </FormGroup>

                                    <FormGroup>
                                        <Col className="rownameinfo" componentClass={ControlLabel} sm={3}>
                                            Минимальное время
                                        </Col>
                                        <Col sm={8}>
                                            <FormControl
                                                placeholder="Минимальное время"
                                                value={this.props.dish.minCookTime}
                                                onChange={
                                                    (event) => {
                                                        this.props.dish.minCookTime = event.target.value;
                                                        this.props.updateDish(this.props.dish);
                                                    }
                                                }
                                            />
                                        </Col>
                                    </FormGroup>

                                    <FormGroup>
                                        <Col className="rownameinfo" componentClass={ControlLabel} sm={3}>
                                            Максимальное время
                                        </Col>
                                        <Col sm={8}>
                                            <FormControl
                                                placeholder="Максимальное время"
                                                value={this.props.dish.maxCookTime}
                                                onChange={
                                                    (event) => {
                                                        this.props.dish.maxCookTime = event.target.value;
                                                        this.props.updateDish(this.props.dish);
                                                    }
                                                }
                                            />
                                        </Col>
                                    </FormGroup>


                                    <FormGroup>
                                        <Col className="rownameinfo" componentClass={ControlLabel} sm={3}>
                                            Специализация
                                        </Col>
                                        <Col sm={8}>
                                            <FormControl
                                                onChange={
                                                    (event) => {
                                                        this.props.dish.specialization = event.target.value;
                                                        this.props.updateDish(this.props.dish);
                                                    }
                                                }
                                                inputRef={el => this.inputEl = el}
                                                componentClass="select"
                                                value={this.props.dish.specialization}
                                            >

                                                <option value="MEAT">Мясо</option>
                                                <option value="FISH">Рыба</option>
                                                <option value="DESSERT">Выпечка</option>
                                            </FormControl>
                                        </Col>
                                    </FormGroup>

                                    <FormGroup className="textright">
                                        <Col smOffset={9} sm={2}>
                                            <Button
                                                bsStyle="success"
                                                onClick={
                                                    () => {
                                                        console.log('click');
                                                        this.props.saveChanges();
                                                    }
                                                }
                                            >
                                                Сохранить</Button>
                                        </Col>
                                    </FormGroup>
                                </Form>
                            </div>
                        </Row>
                    </div>
                </div>
            </Grid>

        );
    }
}

class ChoseIngredientsPlate extends React.Component {

    render() {
        return (
            <Grid>
                <div className="infoblock">
                    <div className="infoblockname">Основные ингредиенты</div>
                    <div>
                        <Row>
                            <div>
                                <Form horizontal>
                                    <FormGroup>

                                        <Col mdOffset={1} md={10}>
                                            <IngredientsTable className="textleft"
                                                              editIngredient={this.props.editIngredient}
                                                              deleteIngredient={this.props.deleteIngredient}
                                                              addIngredient={this.props.addIngredient}
                                                              ingredients={this.props.ingredients}
                                            >
                                            </IngredientsTable>
                                            <Button className="textleft" style={{fontSize: 20}}
                                                    onClick={() => {
                                                        this.props.addDishIngredient()
                                                    }}
                                            >
                                                Новый ингредиент
                                            </Button>
                                        </Col>

                                    </FormGroup>
                                </Form>
                            </div>

                        </Row>

                    </div>
                </div>

            </Grid>

        );
    }
}

class IngredientsTable extends React.Component {

    render() {

        let rows = this.props.ingredients.map(
            (ingredient, rowId) => {
                return <IngredientRow
                    key={rowId}
                    data={ingredient}
                    rowId={rowId}
                    editIngredient={this.props.editIngredient}
                    deleteIngredient={this.props.deleteIngredient}
                    addIngredient={this.props.addIngredient}
                />
            }
        );

        return (
            <Table striped bordered condensed hover>
                <thead>
                <tr>
                    <th className="td_1">#</th>
                    <th className="td_2">Название</th>
                    <th>Количество</th>
                </tr>
                </thead>
                <tbody>
                {rows}
                </tbody>
            </Table>
        );
    }
}

class IngredientRow extends React.Component {

    render() {
        return (
            <tr className="textleft">
                <td className="td_1">{this.props.rowId + 1}</td>
                <td>{this.props.data.synonyms.length === 0 ? 'Ингредиент не выбран' : this.props.data.synonyms[0].name}</td>
                <td>{this.props.data.synonyms.length === 0 ? '-' : this.props.data.synonyms[0].count}</td>
                <td className="td_btn">
                    <img
                        className="binimage"
                        alt=""
                        src="/edit.jpg"
                        onClick={() => {
                            this.props.editIngredient(this.props.rowId)
                        }}
                    />
                </td>
                <td className="td_btn">
                    <img
                        className="binimage"
                        alt=""
                        src="/delete.png"
                        onClick={() => {
                            this.props.deleteIngredient(this.props.rowId)
                        }}
                    />
                </td>
            </tr>
        );
    }
}

class EditIngredientPlate extends React.Component {

    render() {
        return (
            <Grid>
                <div className="infoblock">
                    <div className="infoblockname">Замены ингредиента</div>
                    <div>
                        <Row>
                            <div>
                                <Form horizontal>
                                    <FormGroup>
                                        <Col mdOffset={1} md={10}>
                                            <EditIngredientTable
                                                updateIngredients={
                                                    (ingredients) => {
                                                        this.props.updateIngredients(ingredients)
                                                    }
                                                }
                                                ingredients={this.props.ingredients}
                                            >
                                            </EditIngredientTable>
                                            <Button className="textleft" style={{fontSize: 20}}
                                                    onClick={() => {
                                                        this.props.addIngredient()
                                                    }}
                                            >
                                                Новая замена
                                            </Button>
                                        </Col>

                                    </FormGroup>
                                </Form>
                            </div>
                        </Row>

                    </div>
                </div>
            </Grid>
        );
    }
}

class EditIngredientTable extends React.Component {

    render() {
        console.log("EditIngredientTable.render");
        let rows = this.props.ingredients.map(
            (ingredient, rawId) => {
                return <EditIngredientRow
                    key={rawId}
                    data={ingredient}
                    rawId={rawId}
                    updateIngredient={
                        (ingredient) => {
                            let copy = this.props.ingredients.slice();
                            copy[ingredient.rowId] = ingredient;
                            this.props.updateIngredients(copy);
                        }
                    }
                    deleteIngredient={
                        (ingredient) => {

                            let copy = this.props.ingredients.slice();
                            copy.splice(ingredient.id - 1, 1);
                            this.props.updateIngredients(copy);
                        }
                    }
                    addIngredient={this.props.addIngredient}
                />
            }
        );

        return (
            <Table striped bordered condensed hover>
                <thead>
                <tr>
                    <th className="td_1">#</th>
                    <th className="td_2">Название</th>
                    <th>Количество</th>
                </tr>
                </thead>
                <tbody>
                {rows}
                </tbody>
            </Table>
        );
    }
}

class EditIngredientRow extends React.Component {

    render() {
        return (
            <tr>
                <td className="td_1">{this.props.rawId + 1}</td>
                <td>
                    <input
                        className="ingredientinput"
                        value={this.props.data.name}
                        onChange={
                            (event) => {
                                this.props.updateIngredient({
                                    rowId: this.props.rawId,
                                    id: this.props.data.id,
                                    name: event.target.value,
                                    count: this.props.data.count
                                })
                            }
                        }
                    />
                </td>
                <td>
                    <input
                        className="ingredientinput"
                        value={this.props.data.count}
                        onChange={
                            (event) => {
                                this.props.updateIngredient({
                                    rowId: this.props.rawId,
                                    id: this.props.data.id,
                                    name: this.props.data.name,
                                    count: event.target.value
                                })
                            }
                        }
                    /></td>
                <td className="td_btn">
                    <img
                        className="binimage"
                        alt=""
                        src="/delete.png"
                        onClick={
                            () => {
                                this.props.deleteIngredient(this.props.rawId)
                            }
                        }
                    />
                </td>
            </tr>
        );
    }
}

export default ManagerPage;
