import React from 'react';

import {Redirect} from 'react-router';
import Cookies from 'universal-cookie';

class StartPage extends React.Component {
    render() {
        const cookies = new Cookies();

        if (cookies.get('username') !== undefined &&
            cookies.get('role') !== undefined &&
            cookies.get('realName') !== undefined) {

            if (cookies.get('role') === 'ADMIN') {
                return (<Redirect to="/admin"/>);
            } else if (cookies.get('role') === 'COOK') {
                return (<Redirect to="/cook"/>);
            } else if (cookies.get('role') === 'MANAGER') {
                return (<Redirect to="/manager"/>);
            }
        } else {
            return (<Redirect to="/login"/>);
        }
    }
}

export default StartPage;
