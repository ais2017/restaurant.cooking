import React from 'react';
import axios from 'axios';

import {Redirect} from 'react-router';
import Cookies from 'universal-cookie';
import {Form, FormGroup, Col, FormControl, Button} from 'react-bootstrap';

class LoginPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userName: '',
            password: ''
        };

        this.handleUserNameChange = this.handleUserNameChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleUserNameChange(event) {
        this.setState({userName: event.target.value});
    }

    handlePasswordChange(event) {
        this.setState({password: event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();

        axios.post('http://localhost:8080/auth/token',
            {
                userName: this.state.userName,
                password: this.state.password
            }
        ).then(
            (res) => {

                const cookies = new Cookies();

                cookies.set('username', res.data.userName);
                cookies.set('realName', res.data.realName);
                cookies.set('role', res.data.role);
                cookies.set('token', res.data.token);

                this.setState(this.state);
            },
            (error) => {
                alert("Неверная пара логин пароль ")
            })
    }

    render() {
        const cookies = new Cookies();

        if (cookies.get('username') !== undefined &&
            cookies.get('role')  !== undefined &&
            cookies.get('realName')  !== undefined) {

            if (cookies.get('role') === 'ADMIN') {
                return (<Redirect to="/admin"/>);
            } else if (cookies.get('role') === 'COOK') {
                return (<Redirect to="/cook"/>);
            } else if (cookies.get('role') === 'MANAGER') {
                return (<Redirect to="/manager"/>);
            }
        } else {

            return (
                <div className="loginContainer">
                    <img alt="" className="icon" src="/icon.png" width="240" height="200"/>
                    <p className="textsenter">Добро пожаловать!</p>
                    <Form horizontal>
                        <FormGroup controlId="formHorizontalEmail">
                            <Col smOffset={1} sm={10}>
                                <FormControl className="logindFormRaw" placeholder="Имя пользователя"
                                             value={this.state.userName} onChange={this.handleUserNameChange}/>
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalPassword">
                            <Col smOffset={1} sm={10}>
                                <FormControl className="logindFormRaw" type="password" placeholder="Пароль"
                                             value={this.state.password} onChange={this.handlePasswordChange}/>
                            </Col>
                        </FormGroup>

                        <FormGroup>
                            <Col smOffset={0}>
                                <Button
                                    className="logindFormRaw logindFormRawButton"
                                    bsStyle="primary"
                                    type="submit"
                                    onClick={this.handleSubmit}> Вход
                                </Button>
                            </Col>
                        </FormGroup>
                    </Form>
                </div>
            );
        }
    }
}

export default LoginPage;
