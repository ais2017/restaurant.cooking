import React from "react";

import {Redirect} from 'react-router';
import Cookies from 'universal-cookie';

class LoginCheck extends React.Component {

    constructor(props) {
        super(props);

        const cookies = new Cookies();

        this.state = {
            token: cookies.get('username'),
            role: cookies.get('role'),
            userRole: this.props.userRole
        };
    }

    componentWillReceiveProps(nextProps, nextContext) {
        const cookies = new Cookies();

        this.setState({
            token: cookies.get('username'),
            role: cookies.get('role'),
            userRole: this.props.userRole
        });
    }

    render() {
        if (this.state.token === undefined ||
            this.state.role  === undefined ||
            this.props.userRole !== this.state.role) {

            return (<Redirect to="/login"/>);
        } else {
            return this.props.elem;
        }
    }
}

export default LoginCheck;
