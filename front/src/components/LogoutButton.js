import React from "react";
import { withRouter } from 'react-router-dom';

import { Button } from 'react-bootstrap';
import Cookies from 'universal-cookie';

class LogoutButton extends React.Component {
  constructor(props) {
    super(props);

    this.handleLogount = this.handleLogount.bind(this);
  }

  handleLogount(event) {

    const cookies = new Cookies();

    cookies.remove('username');

    event.preventDefault();
    this.props.history.push('/');
  }

  render() {
    return (
      <div>
        <Button
          onClick={this.handleLogount}
        >
          Выход
        </Button>
      </div>
    )
  }
}

export default withRouter(LogoutButton);
