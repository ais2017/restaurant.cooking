import React from "react";

import { Col, Row, Grid } from 'react-bootstrap';

import LogoutButton from '../components/LogoutButton';

class Header extends React.Component {
  render() {
    return (
      <div className="myheader">
        <Grid>
          <Row className="show-grid">
            <Col className="textleft" md={1}>
            <img alt="" className="iconmini" src="/mini_icon.png" width="50" height="45" />
            </Col>
            <Col className="textleft" md={8}>
            <small> { this.props.text } </small>
            </Col>
            <Col className="textright" md={3}>
            <LogoutButton />
            </Col>
          </Row>
        </Grid>
      </div>);
  }
}

export default Header;
