package my.cooking.service;

import my.cooking.model.Cook;
import my.cooking.model.Dish;
import my.cooking.model.Order;
import my.cooking.model.enums.OrderStatusEnum;
import my.cooking.model.enums.SpecializationEnum;
import my.cooking.repository.CookRepository;
import my.cooking.repository.OrderDishRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyObject;

@ExtendWith(MockitoExtension.class)
public class OrderQueueServiceTest {

    OrderQueueService orderQueueService;
    @Mock
    private OrderDishRepository orderDishRepository;
    @Mock
    private CookRepository cookRepository;
    private Dish dish1;
    private Dish dish2;

    private Order order1;
    private Order order2;

    private List<Order> orderList;

    private Answer<Object> answerSave;

    @BeforeEach
    void init() {
        orderQueueService = new OrderQueueServiceImpl(orderDishRepository, cookRepository);

        dish1 = new Dish();
        dish1.setSpecialization(SpecializationEnum.FISH);
        dish2 = new Dish();
        dish2.setSpecialization(SpecializationEnum.MEAT);

        order1 = new Order();
        order1.setDish(dish1);
        order2 = new Order();
        order2.setDish(dish2);

        orderList = new ArrayList<>();
        orderList.add(order1);
        orderList.add(order2);

        answerSave = invocation -> invocation.getArguments()[0];
    }

    @Test
    void processNextOrderSuccessFirst() {

        Mockito.when(orderDishRepository.findOneForEachWhereStatusIsWaitingTypeSortedByPriorityAndCreatedTime())
                .thenReturn(orderList);

        Cook cook = new Cook();
        cook.getSpecializationSet().add(SpecializationEnum.FISH);
        cook.setCooking(false);

        Mockito.when(cookRepository.findNotBusyBySpesializationNotInRefusalSetLimit1(dish1.getSpecialization(), order1))
                .thenReturn(cook);

        cook.setCooking(true);
        Mockito.when(cookRepository.save(anyObject()))
                .thenAnswer(answerSave);

        order1.setCook(cook);
        order1.setStatus(OrderStatusEnum.IN_PROCESS);

        Mockito.when(orderDishRepository.save(anyObject()))
                .thenAnswer(answerSave);

        boolean result = orderQueueService.processNext();

        assertTrue(result);
    }

    @Test
    void processNextOrderSuccessSecond() {

        Mockito.when(orderDishRepository.findOneForEachWhereStatusIsWaitingTypeSortedByPriorityAndCreatedTime())
                .thenReturn(orderList);

        Cook cook = new Cook();
        cook.getSpecializationSet().add(SpecializationEnum.MEAT);
        cook.setCooking(false);


        Mockito.when(cookRepository.findNotBusyBySpesializationNotInRefusalSetLimit1(anyObject(), anyObject()))
                .thenReturn(null, cook);

        cook.setCooking(true);
        Mockito.when(cookRepository.save(anyObject()))
                .thenAnswer(answerSave);

        order2.setCook(cook);
        order2.setStatus(OrderStatusEnum.IN_PROCESS);
        Mockito.when(orderDishRepository.save(anyObject()))
                .thenAnswer(answerSave);

        boolean result = orderQueueService.processNext();

        assertTrue(result);
    }

    @Test
    void processNextOrderEmptyQueue() {

        List<Order> orderList = new ArrayList<>();

        Mockito.when(orderDishRepository.findOneForEachWhereStatusIsWaitingTypeSortedByPriorityAndCreatedTime())
                .thenReturn(orderList);

        boolean result = orderQueueService.processNext();

        assertFalse(result);
    }

    @Test
    void processNextOrderCooksDidNotFound() {

        Mockito.when(orderDishRepository.findOneForEachWhereStatusIsWaitingTypeSortedByPriorityAndCreatedTime())
                .thenReturn(orderList);


        Mockito.when(cookRepository.findNotBusyBySpesializationNotInRefusalSetLimit1(anyObject(), anyObject()))
                .thenReturn(null);

        boolean result = orderQueueService.processNext();

        assertFalse(result);
    }
}
