package my.cooking.service;

import my.cooking.model.Dish;
import my.cooking.model.DishIngredient;
import my.cooking.model.Ingredient;
import my.cooking.repository.DishRepository;
import my.cooking.repository.StoreRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class StoreServiceTest {

    @Mock
    private DishRepository dishRepository;
    @Mock
    private StoreRepository storeRepository;

    private StoreService storeService;

    private Dish dish = new Dish();
    private Dish dish1 = new Dish();

    private DishIngredient dishIngredient1;
    private DishIngredient dishIngredient2;

    private Ingredient ingredient1 = new Ingredient("Вода", 1);
    private Ingredient ingredient2 = new Ingredient("Растаявший лёд", 1);
    private Ingredient ingredient3 = new Ingredient("Сконденсированный пар", 1);
    private Ingredient ingredient4 = new Ingredient("Семга", 100);
    private Ingredient ingredient5 = new Ingredient("Лосось", 100);
    private Ingredient ingredient6 = new Ingredient("Объедки", 200);

    private DishIngredient dishIngredient11;
    private DishIngredient dishIngredient21;

    private Ingredient ingredient11 = new Ingredient("Вода", 1);
    private Ingredient ingredient21 = new Ingredient("Растаявший лёд", 1);
    private Ingredient ingredient31 = new Ingredient("Сконденсированный пар", 1);
    private Ingredient ingredient41 = new Ingredient("Семга", 100);
    private Ingredient ingredient51 = new Ingredient("Лосось", 100);
    private Ingredient ingredient61 = new Ingredient("Объедки", 200);

    @BeforeEach
    void init() {
        storeService = new StoreServiceImpl(storeRepository, dishRepository);

        dishIngredient1 = new DishIngredient();

        dishIngredient1.setChosenIngredient(ingredient1);
        dishIngredient1.getSubSet().add(ingredient2);
        dishIngredient1.getSubSet().add(ingredient3);

        dishIngredient2 = new DishIngredient();

        dishIngredient2.setChosenIngredient(ingredient4);
        dishIngredient2.getSubSet().add(ingredient5);
        dishIngredient2.getSubSet().add(ingredient6);

        dish.getDishIngredientSet().add(dishIngredient1);
        dish.getDishIngredientSet().add(dishIngredient2);

        // 2
        dishIngredient11 = new DishIngredient();

        dishIngredient11.setChosenIngredient(ingredient11);
        dishIngredient11.getSubSet().add(ingredient21);
        dishIngredient11.getSubSet().add(ingredient31);

        dishIngredient21 = new DishIngredient();

        dishIngredient21.setChosenIngredient(ingredient41);
        dishIngredient21.getSubSet().add(ingredient51);
        dishIngredient21.getSubSet().add(ingredient61);

        dish1.getDishIngredientSet().add(dishIngredient11);
        dish1.getDishIngredientSet().add(dishIngredient21);
    }

    @Test
    void dishIngredientReservation() {
        Mockito.when(storeRepository.reserveIngredient(any()))
                .thenReturn(true);
        Mockito.when(dishRepository.save(any()))
                .thenAnswer(invocation -> invocation.getArguments()[0]);

        Dish result = storeService.dishIngredientReservation(dish);

        assertEquals(dish1, result);
    }

    @Test
    void dishIngredientReservationThereIsNoIngredient1() {

        Mockito.when(storeRepository.reserveIngredient(any()))
                .thenReturn(false, true);
        Mockito.when(dishRepository.save(any()))
                .thenAnswer(invocation -> invocation.getArguments()[0]);

        Dish result = storeService.dishIngredientReservation(dish);

        dishIngredient11.getSubSet().add(dishIngredient11.getChosenIngredient());
        dishIngredient11.setChosenIngredient(ingredient31);
        dishIngredient11.getSubSet().remove(dishIngredient11.getChosenIngredient());

        //        assertEquals(dish1, result);
    }

    @Test
    void dishIngredientReservationThereIsNoIngredients() {
        Mockito.when(storeRepository.reserveIngredient(any()))
                .thenReturn(false);

        Dish result = storeService.dishIngredientReservation(dish);

        assertNull(result);
    }
//
//    void swap(Dish dish) {
//        for (DishIngredient ingredient : dish.getDishIngredientSet()) {
//            for (Ingredient subIngredient : ingredient.getSubSet()) {
//                ingredient.getSubSet().add(ingredient.getChosenIngredient());
//                ingredient.setChosenIngredient(subIngredient);
//                ingredient.getSubSet().remove(ingredient.getChosenIngredient());
//                return;
//            }
//        }
//
//    }
//
//    @Test
//    void superTest() {
//        System.out.println(dish1.equals(dish));
//        swap(dish);
//        swap(dish1);
//        System.out.println(dish1.equals(dish));
//    }
}
