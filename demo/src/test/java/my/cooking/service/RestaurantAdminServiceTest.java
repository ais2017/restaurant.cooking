package my.cooking.service;

import my.cooking.model.Cook;
import my.cooking.model.Dish;
import my.cooking.model.DishIngredient;
import my.cooking.model.Ingredient;
import my.cooking.model.enums.SpecializationEnum;
import my.cooking.repository.CookRepository;
import my.cooking.repository.DishRepository;
import my.cooking.repository.OrderDishRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyObject;

@ExtendWith(MockitoExtension.class)
public class RestaurantAdminServiceTest {

    @Mock
    private DishRepository dishRepository;
    @Mock
    private CookRepository cookRepository;
    @Mock
    private OrderDishRepository orderDishRepository;

    private DishService dishService;
    private CookService cookService;

    private Dish dish = new Dish();
    private Cook cook = new Cook();

    private Dish dishCopy = new Dish();
    private Cook cookCopy = new Cook();

    private Answer<Object> answerSave;

    @BeforeEach
    void init() {
        dishService = new DishServiceImpl(dishRepository);
        cookService = new CookServiceImpl(cookRepository, orderDishRepository);

        Ingredient ingredient11 = new Ingredient("Вода", 10);
        Ingredient ingredient12 = new Ingredient("Рыба", 1);
        Ingredient ingredient13 = new Ingredient("Вода", 2);

        Ingredient ingredient21 = new Ingredient("Вода", 10);
        Ingredient ingredient22 = new Ingredient("Рыба", 1);
        Ingredient ingredient23 = new Ingredient("Вода", 2);

        ArrayList<Ingredient> ingredientSet1 = new ArrayList<>();
        ingredientSet1.add(ingredient13);

        DishIngredient dishIngredient11 = new DishIngredient(ingredient11);
        DishIngredient dishIngredient12 = new DishIngredient(ingredient12, ingredientSet1);

        ArrayList<Ingredient> ingredientSet2 = new ArrayList<>();
        ingredientSet2.add(ingredient23);

        DishIngredient dishIngredient21 = new DishIngredient(ingredient21);
        DishIngredient dishIngredient22 = new DishIngredient(ingredient22, ingredientSet2);

        dish = new Dish("Слаборыбный суп", SpecializationEnum.FISH, 1, 10);
        dish.getDishIngredientSet().add(dishIngredient11);
        dish.getDishIngredientSet().add(dishIngredient12);

        dishCopy = new Dish("Слаборыбный суп", SpecializationEnum.FISH, 1, 10);
        dishCopy.getDishIngredientSet().add(dishIngredient21);
        dishCopy.getDishIngredientSet().add(dishIngredient22);

        cook = new Cook("Ваня", 10, new HashSet<>(Arrays.asList(SpecializationEnum.FISH)));
        cookCopy = new Cook("Ваня", 10, new HashSet<>(Arrays.asList(SpecializationEnum.FISH)));

        answerSave = invocation -> invocation.getArguments()[0];
    }

    @Test
    void editDishSuccess() {
        Mockito.when(dishRepository.save(anyObject()))
                .thenAnswer(answerSave);

        Dish result = dishService.editDish(dish);
        assertEquals(dish, result);
    }

    @Test
    void editDishFail() {
        Mockito.when(dishRepository.save(anyObject()))
                .thenReturn(null);
        Dish result = dishService.editDish(dish);
        assertNull(result);
    }

    @Test
    void addDishSuccess() {
        Mockito.when(dishRepository.save(anyObject()))
                .thenAnswer(answerSave);

        Dish result = dishService.addDish(dish);
        assertEquals(dish, result);
        assertArrayEquals(dish.getDishIngredientSet().toArray(), result.getDishIngredientSet().toArray());

        assertEquals(dish.getAvgCookingTime(), result.getAvgCookingTime());
        assertEquals(dish.getMinCookingTime(), result.getMinCookingTime());
        assertEquals(dish.getMaxCookingTime(), result.getMaxCookingTime());
        assertEquals(dish.getName(), result.getName());
        assertEquals(dish.getSpecialization(), result.getSpecialization());
    }

    @Test
    void addDishFail() {
        Mockito.when(dishRepository.save(anyObject()))
                .thenReturn(null);
        Dish result = dishService.addDish(dish);
        assertNull(result);
    }

    @Test
    void removeDishSuccess() {
        Mockito.doNothing().when(dishRepository).delete(dishCopy);
        assertDoesNotThrow(() -> dishService.removeDish(dish));
    }

    @Test
    void editCookSuccess() {
        Mockito.when(cookRepository.save(anyObject()))
                .thenAnswer(answerSave);

        Cook result = cookService.editCook(cook);
        assertEquals(cook, result);
    }

    @Test
    void editCookFail() {
        Mockito.when(cookRepository.save(anyObject()))
                .thenReturn(null);
        Cook result = cookService.editCook(cook);
        assertNull(result);
    }

    @Test
    void addCookSuccess() {
        Mockito.when(cookRepository.save(anyObject()))
                .thenAnswer(answerSave);

        Cook result = cookService.addCook(cook);
        assertEquals(cook, result);
    }

    @Test
    void addCookFail() {
        Mockito.when(cookRepository.save(anyObject()))
                .thenReturn(null);

        Cook result = cookService.addCook(cook);
        assertNull(result);
    }

    @Test
    void removeCookSuccess() {
        Mockito.doNothing().when(cookRepository)
                .delete(cook);

        assertDoesNotThrow(() -> cookService.removeCook(cook));
    }
}
