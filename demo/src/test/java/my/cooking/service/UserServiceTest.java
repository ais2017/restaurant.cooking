package my.cooking.service;

import my.cooking.model.Cook;
import my.cooking.model.User;
import my.cooking.model.enums.RoleEnum;
import my.cooking.repository.CookRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    private UserService userService;
    @Mock
    private CookRepository cookRepository;

    private AdminService adminService;

    private User userBeforeSave;
    private User userAfterSave;

    private User cookUserBeforeSave;
    private User cookUserAfterSave;

    private Cook cook;

    @BeforeEach
    void init() throws NoSuchFieldException, IllegalAccessException {

        adminService = new AdminServiceImpl(userService, cookRepository);

        userBeforeSave = new User();
        userBeforeSave.setUserName("Hello");
        userBeforeSave.setPassword("Password");
        userBeforeSave.setRole(RoleEnum.ADMIN);

        cookUserBeforeSave = new User();
        cookUserBeforeSave.setUserName("HelloCook");
        cookUserBeforeSave.setPassword("Password1");
        cookUserBeforeSave.setRole(RoleEnum.COOK);

        cookUserAfterSave = new User();
        cookUserAfterSave.setUserName("HelloCook");
        cookUserAfterSave.setPassword("ASASFASGASG");
        cookUserAfterSave.setRole(RoleEnum.COOK);

        userAfterSave = new User();

        Field field = userAfterSave.getClass().getDeclaredField("id");
        field.setAccessible(true);
        field.set(userAfterSave, 10);

        userAfterSave.setUserName("Hello");
        userAfterSave.setPassword("ASDAWAa#!@$ASFAS");
        userAfterSave.setRole(RoleEnum.ADMIN);

        cook = new Cook();
        cook.setUserId(10);
    }

    @Test
    void findUserByName() {
        Mockito.when(userService.findByName(userBeforeSave.getUserName()))
                .thenReturn(userAfterSave);

        User result = adminService.findByName(userBeforeSave.getUserName());

        assertEquals(userAfterSave.getId(), result.getId());
        assertEquals(userBeforeSave.getUserName(), result.getUserName());
        assertNotEquals(userBeforeSave.getPassword(), result.getPassword());
        assertEquals(userBeforeSave.getRole(), result.getRole());
    }

    @Test
    void addUser() {
        Mockito.when(userService.addUser(any()))
                .thenReturn(userAfterSave);

        User result = adminService.addUser(userBeforeSave);

        assertEquals(userAfterSave.getId(), result.getId());
        assertEquals(userBeforeSave.getUserName(), result.getUserName());
        assertNotEquals(userBeforeSave.getPassword(), result.getPassword());
        assertEquals(userBeforeSave.getRole(), result.getRole());
    }

    @Test
    void addUserWasACook() {
        User result = adminService.addUser(cookUserBeforeSave);

        assertNull(result);
    }

    @Test
    void addCookUser() {
        Mockito.when(cookRepository.save(any()))
                .thenAnswer(invocation -> invocation.getArguments()[0]);
        Mockito.when(userService.addUser(cookUserBeforeSave))
                .thenReturn(cookUserAfterSave);

        User result = adminService.addUser(cookUserBeforeSave, cook);

        assertEquals(cook.getUserId(), result.getId());
        assertEquals(cookUserBeforeSave.getRole(), result.getRole());
        assertEquals(cookUserBeforeSave.getUserName(), result.getUserName());
        assertEquals(cookUserAfterSave.getPassword(), result.getPassword());
    }

    @Test
    void addCookUserWasNotACook() {
        User result = adminService.addUser(userBeforeSave, cook);

        assertNull(result);
    }

    @Test
    void addUserUserDidNotFound() {
        Mockito.when(userService.addUser(userBeforeSave))
                .thenReturn(null);

        User result = userService.addUser(userBeforeSave);

        assertNull(result);
    }

    @Test
    void changeUser() {
        Mockito.when(userService.editUser(any()))
                .thenAnswer(invocation -> invocation.getArguments()[0]);

        User result = adminService.editUser(userAfterSave);

        assertEquals(userAfterSave.getId(), result.getId());
        assertEquals(userAfterSave.getUserName(), result.getUserName());
        assertEquals(userAfterSave.getPassword(), result.getPassword());
        assertEquals(userAfterSave.getRole(), result.getRole());
    }

    @Test
    void changeUserWasACook() {
        User result = adminService.editUser(cookUserAfterSave);

        assertNull(result);
    }

    @Test
    void changeCookUser() {
        Mockito.when(userService.editUser(any()))
                .thenAnswer(invocation -> invocation.getArguments()[0]);
        Mockito.when(cookRepository.save(any()))
                .thenAnswer(invocation -> invocation.getArguments()[0]);
        User result = adminService.editUser(cookUserAfterSave, cook);

        assertEquals(cookUserAfterSave.getId(), result.getId());
        assertEquals(cookUserAfterSave.getUserName(), result.getUserName());
        assertEquals(cookUserAfterSave.getPassword(), result.getPassword());
        assertEquals(cookUserAfterSave.getRole(), result.getRole());
    }

    @Test
    void changeCookUserWasNotACook() {
        User result = adminService.editUser(userAfterSave, cook);

        assertNull(result);
    }
}
