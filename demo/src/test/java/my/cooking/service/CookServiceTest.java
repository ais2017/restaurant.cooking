package my.cooking.service;

import my.cooking.model.Cook;
import my.cooking.model.Order;
import my.cooking.model.enums.OrderStatusEnum;
import my.cooking.repository.CookRepository;
import my.cooking.repository.OrderDishRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyObject;

@ExtendWith(MockitoExtension.class)
public class CookServiceTest {

    Answer<Object> answerSave;
    @Mock
    private CookRepository cookRepository;
    @Mock
    private OrderDishRepository orderDishRepository;
    private CookService cookService;
    private Cook cook;
    private Order order1;
    private Order order2;

    @BeforeEach
    void init() {
        cookService = new CookServiceImpl(cookRepository, orderDishRepository);

        cook = new Cook();
        order1 = new Order();
        order1.setStatus(OrderStatusEnum.IN_PROCESS);

        order2 = new Order();
        order2.setCook(cook);
        order2.setStatus(OrderStatusEnum.COOK);

        answerSave = invocation -> invocation.getArguments()[0];
    }

    @Test
    void checkNextOrderSuccess() {
        Mockito.when(orderDishRepository.findByCookId(cook.getId()))
                .thenReturn(order1);

        Order result = cookService.findNextOrder(cook.getId());
        assertEquals(order1, result);
    }

    @Test
    void checkNextOrderOrderDidNotFound() {
        Mockito.when(orderDishRepository.findByCookId(cook.getId()))
                .thenReturn(null);

        Order result = cookService.findNextOrder(cook.getId());
        assertNull(result);
    }

    @Test
    void takeOrder() {
        Mockito.when(orderDishRepository.findOne(order1.getId()))
                .thenReturn(order1);
        Mockito.when(cookRepository.findOne(cook.getId()))
                .thenReturn(cook);
        Mockito.when(orderDishRepository.save(anyObject()))
                .thenAnswer(answerSave);

        Order result = cookService.takeOrder(cook.getId(), order1.getId());

        assertEquals(OrderStatusEnum.COOK, result.getStatus());
        assertEquals(cook, result.getCook());
    }

    @Test
    void takeOrderDishDidNotFound() {
        Mockito.when(orderDishRepository.findOne(order1.getId()))
                .thenReturn(null);

        Order result = cookService.takeOrder(cook.getId(), order1.getId());

        assertNull(result);
    }

    @Test
    void takeOrderCookDidNotFound() {
        Mockito.when(orderDishRepository.findOne(order1.getId()))
                .thenReturn(order1);
        Mockito.when(cookRepository.findOne(cook.getId()))
                .thenReturn(null);

        Order result = cookService.takeOrder(cook.getId(), order1.getId());

        assertNull(result);
    }

    @Test
    void takeOrderDidNotSaved() {
        Mockito.when(orderDishRepository.findOne(order1.getId()))
                .thenReturn(order1);
        Mockito.when(cookRepository.findOne(cook.getId()))
                .thenReturn(cook);
        Mockito.when(orderDishRepository.save(anyObject()))
                .thenReturn(null);

        Order result = cookService.takeOrder(cook.getId(), order1.getId());

        assertNull(result);
    }

    @Test
    void fixEndOfCookingSuccess() {
        Mockito.when(orderDishRepository.findByCookId(cook.getId()))
                .thenReturn(order2);
        Mockito.when(orderDishRepository.save(anyObject()))
                .thenAnswer(answerSave);

        Order result = cookService.fixEndOfCooking(cook.getId());
        assertEquals(OrderStatusEnum.DONE, result.getStatus());
        assertFalse(result.getCook().isCooking());
    }

    @Test
    void fixEndOfCookingDishDidNotFound() {
        Mockito.when(orderDishRepository.findByCookId(cook.getId()))
                .thenReturn(null);

        Order result = cookService.fixEndOfCooking(cook.getId());

        assertNull(result);
    }

    @Test
    void fixEndOfCookingDishDidNotSaved() {
        Mockito.when(orderDishRepository.findByCookId(cook.getId()))
                .thenReturn(order2);
        Mockito.when(orderDishRepository.save(anyObject()))
                .thenReturn(null);

        Order result = cookService.fixEndOfCooking(cook.getId());

        assertNull(result);
    }

    @Test
    void refuseCooking() {
        Mockito.when(orderDishRepository.findByCookId(cook.getId()))
                .thenReturn(order2);
        Mockito.when(orderDishRepository.save(anyObject()))
                .thenAnswer(answerSave);
        Mockito.when(cookRepository.save(anyObject()))
                .thenAnswer(answerSave);

        boolean result = cookService.refuseCooking(cook.getId());

        assertTrue(result);
    }

    @Test
    void refuseCookingDishDidNotFound() {
        Mockito.when(orderDishRepository.findByCookId(cook.getId()))
                .thenReturn(null);

        boolean result = cookService.refuseCooking(cook.getId());

        assertFalse(result);
    }

    @Test
    void refuseCookingOrderDidNotSaved() {
        Mockito.when(orderDishRepository.findByCookId(cook.getId()))
                .thenReturn(order2);
        Mockito.when(orderDishRepository.save(anyObject()))
                .thenReturn(null);

        boolean result = cookService.refuseCooking(cook.getId());

        assertFalse(result);
    }

    @Test
    void refuseCookingCookDidNotSaved() {
        Mockito.when(orderDishRepository.findByCookId(cook.getId()))
                .thenReturn(order2);
        Mockito.when(orderDishRepository.save(anyObject()))
                .thenAnswer(answerSave);
        Mockito.when(cookRepository.save(anyObject()))
                .thenReturn(null);

        boolean result = cookService.refuseCooking(cook.getId());

        assertFalse(result);
    }
}
