package my.cooking.service;

import my.cooking.model.*;
import my.cooking.model.enums.OrderStatusEnum;
import my.cooking.model.enums.SpecializationEnum;
import my.cooking.repository.CookRepository;
import my.cooking.repository.OrderDishRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyObject;

@ExtendWith(MockitoExtension.class)
public class ExternalSystemServiceTest {

    @Mock
    private OrderDishRepository orderDishRepository;
    @Mock
    private CookRepository cookRepository;

    private ExternalSystemService externalSystemService
            = new ExternalSystemServiceImpl(orderDishRepository, cookRepository);

    private Order order = new Order();
    private Order orderCopy = new Order();

    private Cook cook1;
    private Cook cook2;

    private Answer<Object> answerSave;

    @BeforeEach
    void init() {
        externalSystemService
                = new ExternalSystemServiceImpl(orderDishRepository, cookRepository);

        Ingredient ingredient11 = new Ingredient("Вода", 10);
        Ingredient ingredient12 = new Ingredient("Рыба", 1);
        Ingredient ingredient13 = new Ingredient("Вода", 2);

        Ingredient ingredient21 = new Ingredient("Вода", 10);
        Ingredient ingredient22 = new Ingredient("Рыба", 1);
        Ingredient ingredient23 = new Ingredient("Вода", 2);

        ArrayList<Ingredient> ingredientSet1 = new ArrayList<>();
        ingredientSet1.add(ingredient13);

        DishIngredient dishIngredient11 = new DishIngredient(ingredient11);
        DishIngredient dishIngredient12 = new DishIngredient(ingredient12, ingredientSet1);

        ArrayList<Ingredient> ingredientSet2 = new ArrayList<>();
        ingredientSet2.add(ingredient23);

        DishIngredient dishIngredient21 = new DishIngredient(ingredient21);
        DishIngredient dishIngredient22 = new DishIngredient(ingredient22, ingredientSet2);

        Dish dish = new Dish("Слаборыбный суп", SpecializationEnum.FISH, 1, 10);
        dish.getDishIngredientSet().add(dishIngredient11);
        dish.getDishIngredientSet().add(dishIngredient12);

        Dish dishCopy = new Dish("Слаборыбный суп", SpecializationEnum.FISH, 1, 10);
        dishCopy.getDishIngredientSet().add(dishIngredient21);
        dishCopy.getDishIngredientSet().add(dishIngredient22);

        cook1 = new Cook("A", 1);
        cook2 = new Cook("A", 1);

        order = new Order(dish, cook1, 1);
        orderCopy = new Order(dishCopy, cook2, 1);

        answerSave = invocation -> invocation.getArguments()[0];
    }

    @Test
    void addNewDishToOfferSuccess() {
        order.setCreateTime(LocalDateTime.now().toInstant(ZoneOffset.UTC).toEpochMilli());
        orderCopy.setCreateTime(order.getCreateTime());

        Mockito.when(orderDishRepository.save(anyObject()))
                .thenAnswer(answerSave);

        Order result = externalSystemService.offer(order);

        assertArrayEquals(order.getRefusalSet().toArray(), result.getRefusalSet().toArray());
        assertEquals(order.getStatus(), result.getStatus());
        assertEquals(order.getCook(), result.getCook());
        assertEquals(order.getDish(), result.getDish());
        assertEquals(order.getCookingEndTime(), result.getCookingEndTime());
        assertEquals(order.getCookingStartTime(), result.getCookingStartTime());
        assertEquals(order.getCreateTime(), result.getCreateTime());
        assertEquals(order.getPriority(), result.getPriority());
    }

    @Test
    void addNewDishToOfferSaveFail() {
        order.setCreateTime(LocalDateTime.now().toInstant(ZoneOffset.UTC).toEpochMilli());

        Mockito.when(orderDishRepository.save(anyObject()))
                .thenReturn(null);

        Order result = externalSystemService.offer(order);
        assertNull(result);
    }

    @Test
    void updateRatingSuccess() {
        int change = 10;

        Mockito.when(cookRepository.findOne(1))
                .thenReturn(cook1);

        Mockito.when(cookRepository.save(anyObject()))
                .thenAnswer(answerSave);

        Cook result = externalSystemService.updateRating(1, change);

        assertEquals(cook1.recalculateRating(change), result.getRating());
    }

    @Test
    void updateRatingCookDidNotFound() {
        int change = 10;
        Mockito.when(cookRepository.findOne(1))
                .thenReturn(null);

        Cook result = externalSystemService.updateRating(1, change);

        assertNull(result);
    }

    @Test
    void updateRatingCookDidNotSaved() {
        int change = 10;

        Mockito.when(cookRepository.findOne(1))
                .thenReturn(cook1);
        Mockito.when(cookRepository.save(anyObject()))
                .thenReturn(null);
        Cook result = externalSystemService.updateRating(1, change);

        assertNull(result);
    }

    @Test
    void refuseCooking() {
        Long cookingEndTime = order.getCookingEndTime();

        Mockito.when(orderDishRepository.findOne(1)).thenReturn(order);
        order.refuseCooking();

        Mockito.when(orderDishRepository.save(anyObject()))
                .thenAnswer(answerSave);

        Order result = externalSystemService.refuseCooking(1);

        assertEquals(order.getRefusalSet(), result.getRefusalSet());
        assertEquals(OrderStatusEnum.REFUSAL, result.getStatus());
        assertEquals(order.getCook(), result.getCook());
        assertEquals(order.getDish(), result.getDish());
        assertNotEquals(cookingEndTime, result.getCookingEndTime());
        assertEquals(order.getCookingStartTime(), result.getCookingStartTime());
        assertEquals(order.getPriority(), result.getPriority());
    }

    @Test
    void refuseCookingOrderDidNotFound() {
        Mockito.when(orderDishRepository.findOne(1))
                .thenReturn(null);

        Order result = externalSystemService.refuseCooking(1);

        assertNull(result);
    }

    @Test
    void refuseCookingDidNotSaved() {

        Mockito.when(orderDishRepository.findOne(1))
                .thenReturn(order);

        Mockito.when(orderDishRepository.save(anyObject()))
                .thenReturn(null);

        Order result = externalSystemService.refuseCooking(1);

        assertNull(result);
    }

    @Test
    void changeCook() {
        Set<Cook> refusalSet = new HashSet<>(order.getRefusalSet());
        refusalSet.add(cook1);

        Mockito.when(orderDishRepository.findOne(1)).thenReturn(order);
        order.cookRefuse();

        Mockito.when(orderDishRepository.save(anyObject()))
                .thenAnswer(answerSave);

        Order result = externalSystemService.changeCook(1);

        assertArrayEquals(refusalSet.toArray(), result.getRefusalSet().toArray());
        assertEquals(OrderStatusEnum.WAIT, result.getStatus());
        assertEquals(order.getCook(), result.getCook());
        assertEquals(order.getDish(), result.getDish());
        assertEquals(order.getCookingEndTime(), result.getCookingEndTime());
        assertEquals(order.getCookingStartTime(), result.getCookingStartTime());
        assertEquals(order.getPriority(), result.getPriority());
    }

    @Test
    void changeCookOrderDidNotFound() {
        Mockito.when(orderDishRepository.findOne(1))
                .thenReturn(null);

        Order result = externalSystemService.changeCook(1);

        assertNull(result);
    }

    @Test
    void changeCookOrderDidNotSaved() {
        Mockito.when(orderDishRepository.findOne(1)).thenReturn(order);
        order.cookRefuse();
        Mockito.when(orderDishRepository.save(anyObject()))
                .thenReturn(null);
        Order result = externalSystemService.changeCook(1);

        assertNull(result);
    }
}
