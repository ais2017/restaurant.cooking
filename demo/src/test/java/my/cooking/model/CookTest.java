package my.cooking.model;

import my.cooking.model.enums.SpecializationEnum;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class CookTest {

    @Test
    void defaultConstructorTest() {
        Cook cook = new Cook();
        assertNull(cook.getName());
        assertEquals(0, cook.getRating());
        assertArrayEquals(Collections.EMPTY_SET.toArray(), cook.getSpecializationSet().toArray());
    }

    @Test
    void twoArgsConstructorTest() {
        String name = "Cook";
        int rating = 42;

        Cook cook = new Cook(name, rating);

        assertEquals(name, cook.getName());
        assertEquals(rating, cook.getRating());
        assertArrayEquals(Collections.EMPTY_SET.toArray(), cook.getSpecializationSet().toArray());
    }

    @Test
    void allArgsConstructorTest() {
        String name = "Cook";
        int rating = 42;
        Set<SpecializationEnum> set = new HashSet<>();
        set.add(SpecializationEnum.DESSERT);
        set.add(SpecializationEnum.MEAT);

        Cook cook = new Cook(name, rating, set);

        assertEquals(0, cook.getId());
        assertEquals(name, cook.getName());
        assertEquals(rating, cook.getRating());
        assertArrayEquals(set.toArray(), cook.getSpecializationSet().toArray());
    }

    @Test
    void setRating() throws NoSuchFieldException, IllegalAccessException {
        Cook cook = new Cook();
        cook.setRating(42);

        Field ratingField = cook.getClass().getDeclaredField("rating");
        ratingField.setAccessible(true);

        assertEquals(42, ratingField.getInt(cook));
    }

    @Test
    void recalculateRating() {
        Cook cook = new Cook("name", 12, new HashSet<>());

        cook.recalculateRating(10);

        assertEquals(22, cook.getRating());
    }

    @Test
    void setName() throws NoSuchFieldException, IllegalAccessException {
        Cook cook = new Cook();
        cook.setName("new name");

        Field field = cook.getClass().getDeclaredField("name");
        field.setAccessible(true);

        assertEquals("new name", field.get(cook));
    }

    @Test
    void equals() {
        Cook cook1 = new Cook();
        Cook cook2 = new Cook();

        cook1.setName("A");
        cook1.setRating(1);
        cook1.getSpecializationSet().add(SpecializationEnum.MEAT);

        cook2.setName("A");
        cook2.setRating(1);
        cook2.getSpecializationSet().add(SpecializationEnum.MEAT);

        assertEquals(cook1, cook2);
    }

    @Test
    void hashCodeTest() {
        Cook cook1 = new Cook();

        cook1.setName("A");
        cook1.setRating(1);
        cook1.getSpecializationSet().add(SpecializationEnum.MEAT);

        assertEquals(Objects.hash(cook1.getName(), cook1.isCooking(), cook1.getRating(), cook1.getSpecializationSet()), cook1.hashCode());
    }
}