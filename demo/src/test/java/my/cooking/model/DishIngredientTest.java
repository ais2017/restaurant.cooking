package my.cooking.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class DishIngredientTest {

    Ingredient ingredient12;
    Ingredient ingredient13;
    Ingredient ingredient22;
    Ingredient ingredient23;

    DishIngredient dishIngredient12;
    DishIngredient dishIngredient22;

    @BeforeEach
    void init() {
        ingredient12 = new Ingredient("Рыба", 1);
        ingredient13 = new Ingredient("Вода", 2);

        ingredient22 = new Ingredient("Рыба", 1);
        ingredient23 = new Ingredient("Вода", 2);

        ArrayList<Ingredient> ingredientSet1 = new ArrayList<>();
        ingredientSet1.add(ingredient13);

        dishIngredient12 = new DishIngredient(ingredient12, ingredientSet1);

        ArrayList<Ingredient> ingredientSet2 = new ArrayList<>();
        ingredientSet2.add(ingredient23);

        dishIngredient22 = new DishIngredient(ingredient22, ingredientSet2);
    }

    @Test
    void defaultConstructorTest() {
        DishIngredient dishIngredient = new DishIngredient();
        assertNull(dishIngredient.getChosenIngredient());
        assertArrayEquals(Collections.EMPTY_SET.toArray(), dishIngredient.getSubSet().toArray());
    }

    @Test
    void oneArgsConstructorTest() {
        Ingredient chosenIngredient = new Ingredient();

        DishIngredient dishIngredient = new DishIngredient(chosenIngredient);

        assertEquals(chosenIngredient, dishIngredient.getChosenIngredient());
        assertArrayEquals(Collections.EMPTY_SET.toArray(), dishIngredient.getSubSet().toArray());
    }

    @Test
    void twoArgsConstructorTest() {
        Ingredient chosenIngredient = new Ingredient();
        ArrayList<Ingredient> set = new ArrayList<>();
        set.add(chosenIngredient);
        set.add(new Ingredient());

        DishIngredient dishIngredient = new DishIngredient(chosenIngredient, set);

        assertEquals(null, dishIngredient.getId());
        assertEquals(chosenIngredient, dishIngredient.getChosenIngredient());
        assertArrayEquals(set.toArray(), dishIngredient.getSubSet().toArray());
    }

    @Test
    void twoArgsNullConstructorTest() {

        DishIngredient dishIngredient = new DishIngredient(null, new ArrayList<>());

        assertNull(dishIngredient.getChosenIngredient());
        assertArrayEquals(Collections.EMPTY_SET.toArray(), dishIngredient.getSubSet().toArray());
    }

    @Test
    void check() {
        Ingredient ingredient = new Ingredient();

        DishIngredient dishIngredient = new DishIngredient();
        dishIngredient.getSubSet().add(ingredient);

        dishIngredient.setChosenIngredient(ingredient);

        assertTrue(dishIngredient.check());
    }

    @Test
    void checkNull() {
        DishIngredient dishIngredient = new DishIngredient();
        assertFalse(dishIngredient::check);
    }

    @Test
    void setChosenIngredient() throws NoSuchFieldException, IllegalAccessException {
        Ingredient ingredient = new Ingredient();

        DishIngredient dishIngredient = new DishIngredient();
        dishIngredient.getSubSet().add(ingredient);

        dishIngredient.setChosenIngredient(ingredient);

        Field field = dishIngredient.getClass().getDeclaredField("chosenIngredient");
        field.setAccessible(true);

        assertEquals(ingredient, field.get(dishIngredient));
    }

    @Test
    void equals() {
        assertEquals(dishIngredient12, dishIngredient22);
    }

    @Test
    void hashCodeTest() {
        assertEquals(dishIngredient12.hashCode(), dishIngredient22.hashCode());
    }
}