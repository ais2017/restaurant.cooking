package my.cooking.model;

import my.cooking.model.enums.SpecializationEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DishTest {

    Dish defDish;
    DishIngredient dishIngredient1;
    DishIngredient dishIngredient2;

    Dish dish;
    Dish dishCopy;

    @BeforeEach
    void initAll() {
        Ingredient ingredient11 = new Ingredient("Вода", 10);
        Ingredient ingredient12 = new Ingredient("Рыба", 1);
        Ingredient ingredient13 = new Ingredient("Вода", 2);

        Ingredient ingredient21 = new Ingredient("Вода", 10);
        Ingredient ingredient22 = new Ingredient("Рыба", 1);
        Ingredient ingredient23 = new Ingredient("Вода", 2);

        ArrayList<Ingredient> ingredientSet1 = new ArrayList<>();
        ingredientSet1.add(ingredient13);

        DishIngredient dishIngredient11 = new DishIngredient(ingredient11);
        DishIngredient dishIngredient12 = new DishIngredient(ingredient12, ingredientSet1);

        ArrayList<Ingredient> ingredientSet2 = new ArrayList<>();
        ingredientSet2.add(ingredient23);

        DishIngredient dishIngredient21 = new DishIngredient(ingredient21);
        DishIngredient dishIngredient22 = new DishIngredient(ingredient22, ingredientSet2);

        dish = new Dish("Слаборыбный суп", SpecializationEnum.FISH, 1, 10);
        dish.getDishIngredientSet().add(dishIngredient11);
        dish.getDishIngredientSet().add(dishIngredient12);

        dishCopy = new Dish("Слаборыбный суп", SpecializationEnum.FISH, 1, 10);
        dishCopy.getDishIngredientSet().add(dishIngredient21);
        dishCopy.getDishIngredientSet().add(dishIngredient22);

        String name = "Def name";
        SpecializationEnum spec = SpecializationEnum.MEAT;
        double min = 1;
        double max = 4;

        dishIngredient1 = new DishIngredient();
        dishIngredient2 = new DishIngredient(new Ingredient());

        defDish = new Dish(name, spec, min, max);
    }

    @Test
    void defaultConstructorTest() {
        Dish dish = new Dish();

        assertEquals(null, dish.getId());
        assertEquals(0, dish.getAvgCookingTime());
        assertEquals(0, dish.getMinCookingTime());
        assertEquals(0, dish.getMaxCookingTime());
        assertNull(dish.getName());
        assertArrayEquals(Collections.EMPTY_SET.toArray(), dish.getDishIngredientSet().toArray());
        assertNull(dish.getSpecialization());
    }

    @Test
    void fiveArgsConstructorTest() {
        String name = "Def name";
        SpecializationEnum spec = SpecializationEnum.MEAT;
        double min = 1;
        double avg = 2.5;
        double max = 4;
        Dish dish = new Dish(name, spec, min, max);
        assertEquals(avg, dish.getAvgCookingTime());
        assertEquals(min, dish.getMinCookingTime());
        assertEquals(max, dish.getMaxCookingTime());
        assertEquals(name, dish.getName());
        assertEquals(spec, dish.getSpecialization());
        assertArrayEquals(Collections.EMPTY_SET.toArray(), dish.getDishIngredientSet().toArray());
    }

    @Test
    void allArgsConstructorTest() {
        String name = "Def name";
        SpecializationEnum spec = SpecializationEnum.MEAT;
        double min = 1;
        double avg = 2.5;
        double max = 4;

        Set<DishIngredient> set = new HashSet<>();
        set.add(new DishIngredient());
        set.add(new DishIngredient());
        set.add(new DishIngredient());

        Dish dish = new Dish(name, spec, min, max, set);
        assertEquals(avg, dish.getAvgCookingTime());
        assertEquals(min, dish.getMinCookingTime());
        assertEquals(max, dish.getMaxCookingTime());
        assertEquals(name, dish.getName());
        assertEquals(spec, dish.getSpecialization());
        assertArrayEquals(set.toArray(), dish.getDishIngredientSet().toArray());
    }

    @Test
    void checkEmptyIngredients() {
        assertTrue(defDish.checkIngredients());
    }

    @Test
    void checkNullIngredients() {
        defDish.getDishIngredientSet().add(dishIngredient2);
        defDish.getDishIngredientSet().add(null);
        assertThrows(NullPointerException.class, () -> defDish.checkIngredients());
    }

    @Test
    void checkFalseEmptyIngredients() {
        defDish.getDishIngredientSet().add(dishIngredient1);
        assertFalse(defDish.checkIngredients());
    }

    @Test
    void setName() throws NoSuchFieldException, IllegalAccessException {
        String name = "Name";

        Dish dish = new Dish();
        dish.setName(name);

        Field field = dish.getClass().getDeclaredField("name");
        field.setAccessible(true);

        assertEquals(name, field.get(dish));
    }

    @Test
    void setSpecialization() throws NoSuchFieldException, IllegalAccessException {
        SpecializationEnum spec = SpecializationEnum.FISH;

        Dish dish = new Dish();
        dish.setSpecialization(spec);


        Field field = dish.getClass().getDeclaredField("specialization");
        field.setAccessible(true);

        assertEquals(spec, field.get(dish));
    }

    @Test
    void setMinCookingTime() throws NoSuchFieldException, IllegalAccessException {
        double min = 7;
        double avg = 8.5;
        double max = 10;

        Dish dish = new Dish();
        dish.setMinCookingTime(min);


        Field minField = dish.getClass().getDeclaredField("minCookingTime");
        minField.setAccessible(true);

        Field maxField = dish.getClass().getDeclaredField("maxCookingTime");
        maxField.setAccessible(true);
        maxField.set(dish, max);

        Field avgField = dish.getClass().getDeclaredField("avgCookingTime");
        avgField.setAccessible(true);
        avgField.set(dish, avg);

        assertEquals(min, minField.get(dish));
        assertEquals(avg, avgField.get(dish));
    }

    @Test
    void setMaxCookingTime() throws NoSuchFieldException, IllegalAccessException {
        double min = 7;
        double avg = 8.5;
        double max = 10;

        Dish dish = new Dish();
        dish.setMaxCookingTime(max);


        Field minField = dish.getClass().getDeclaredField("minCookingTime");
        minField.setAccessible(true);
        minField.set(dish, min);

        Field maxField = dish.getClass().getDeclaredField("maxCookingTime");
        maxField.setAccessible(true);

        Field avgField = dish.getClass().getDeclaredField("avgCookingTime");
        avgField.setAccessible(true);
        avgField.set(dish, avg);

        assertEquals(max, maxField.get(dish));
        assertEquals(avg, avgField.get(dish));
    }

    @Test
    void equals() {
        assertEquals(dish, dishCopy);
    }

    @Test
    void hashCodeTest() {
        assertEquals(dish.hashCode(), dishCopy.hashCode());
    }
}