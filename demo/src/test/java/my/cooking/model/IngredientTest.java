package my.cooking.model;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class IngredientTest {

    @Test
    void defaultConstructorTest() {
        Ingredient ingredient = new Ingredient();
        assertNull(ingredient.getName());
        assertEquals(0, ingredient.getCount());
    }

    @Test
    void twoArgsConstructorTest() {
        String name = "tomato";
        int count = 42;

        Ingredient ingredient = new Ingredient(name, count);

        assertEquals(null, ingredient.getId());
        assertEquals(name, ingredient.getName());
        assertEquals(count, ingredient.getCount());
    }

    @Test
    void setName() throws NoSuchFieldException, IllegalAccessException {
        Ingredient ingredient = new Ingredient();
        ingredient.setName("new name");

        Field field = ingredient.getClass().getDeclaredField("name");
        field.setAccessible(true);

        assertEquals("new name", field.get(ingredient));
    }

    @Test
    void setCount() throws NoSuchFieldException, IllegalAccessException {
        Ingredient ingredient = new Ingredient();
        ingredient.setCount(10);

        Field field = ingredient.getClass().getDeclaredField("count");
        field.setAccessible(true);

        assertEquals(10, field.get(ingredient));
    }

    @Test
    void equals() {
        Ingredient ingredient1 = new Ingredient();
        Ingredient ingredient2 = new Ingredient();

        ingredient1.setCount(10);
        ingredient1.setName("A");

        ingredient2.setCount(10);
        ingredient2.setName("A");

        assertEquals(ingredient1, ingredient2);
    }

    @Test
    void hashCodeTest() {
        Ingredient ingredient1 = new Ingredient();

        ingredient1.setCount(10);
        ingredient1.setName("A");

        assertEquals(Objects.hash(ingredient1.getName(), ingredient1.getCount()), ingredient1.hashCode());

    }
}