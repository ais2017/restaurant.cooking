package my.cooking.model;

import my.cooking.model.enums.OrderStatusEnum;
import my.cooking.model.enums.SpecializationEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {

    Order order1;
    Order order2;

    @BeforeEach
    void init() {
        Ingredient ingredient11 = new Ingredient("Вода", 10);
        Ingredient ingredient12 = new Ingredient("Рыба", 1);
        Ingredient ingredient13 = new Ingredient("Вода", 2);

        Ingredient ingredient21 = new Ingredient("Вода", 10);
        Ingredient ingredient22 = new Ingredient("Рыба", 1);
        Ingredient ingredient23 = new Ingredient("Вода", 2);

        ArrayList<Ingredient> ingredientSet1 = new ArrayList<>();
        ingredientSet1.add(ingredient13);

        DishIngredient dishIngredient11 = new DishIngredient(ingredient11);
        DishIngredient dishIngredient12 = new DishIngredient(ingredient12, ingredientSet1);

        ArrayList<Ingredient> ingredientSet2 = new ArrayList<>();
        ingredientSet2.add(ingredient23);

        DishIngredient dishIngredient21 = new DishIngredient(ingredient21);
        DishIngredient dishIngredient22 = new DishIngredient(ingredient22, ingredientSet2);

        Dish dish = new Dish("Слаборыбный суп", SpecializationEnum.FISH, 1, 10);
        dish.getDishIngredientSet().add(dishIngredient11);
        dish.getDishIngredientSet().add(dishIngredient12);

        Dish dishCopy = new Dish("Слаборыбный суп", SpecializationEnum.FISH, 1, 10);
        dishCopy.getDishIngredientSet().add(dishIngredient21);
        dishCopy.getDishIngredientSet().add(dishIngredient22);

        Cook cook1 = new Cook("A", 1);
        Cook cook2 = new Cook("A", 1);

        order1 = new Order(dish, cook1, 1);
        order2 = new Order(dishCopy, cook2, 1);
    }

    @Test
    void defaultConstructorTest() {
        Order order = new Order();
        assertNull(order.getCook());
        assertNull(order.getDish());

        assertEquals(0, order.getPriority());
//        assertNull(order.getCookingEndTime());
//        assertNull(order.getCookingStartTime());
        assertEquals(OrderStatusEnum.WAIT, order.getStatus());
        assertArrayEquals(Collections.EMPTY_SET.toArray(), order.getRefusalSet().toArray());
    }

    @Test
    void treeArgsConstructorTest() {
        Dish dish = new Dish();
        Cook cook = new Cook();
        int priority = 1;

        Order order = new Order(dish, cook, priority);

        assertEquals(cook, order.getCook());
        assertEquals(dish, order.getDish());

        assertEquals(1, order.getPriority());
//        assertNull(order.getCookingEndTime());
//        assertNull(order.getCookingStartTime());
        assertEquals(OrderStatusEnum.WAIT, order.getStatus());
        assertArrayEquals(Collections.EMPTY_SET.toArray(), order.getRefusalSet().toArray());
    }

    @Test
    void startCooking() {
        Order order = new Order();

        order.startCooking();

        assertNotNull(order.getCookingStartTime());
        assertEquals(OrderStatusEnum.COOK, order.getStatus());
    }

    @Test
    void endCooking() {
        Order order = new Order();
        order.setStatus(OrderStatusEnum.WAIT);

        order.endCooking();

        assertNotNull(order.getCookingEndTime());
        assertEquals(OrderStatusEnum.DONE, order.getStatus());
    }

    @Test
    void cookRefuseByExternalSystem() {
        Cook cook = new Cook();

        Order order = new Order();
        order.setCook(cook);
        order.setDish(new Dish());
        order.cookRefuse();

        Set<Cook> set = order.getRefusalSet();

        Set<Cook> set1 = new HashSet<>(set);

        assertArrayEquals(set1.toArray(), order.getRefusalSet().toArray());
        assertEquals(cook, order.getCook());
        assertNotNull(order.getDish());
        assertEquals(OrderStatusEnum.WAIT, order.getStatus());
    }

    @Test
    void orderCancel() {
        Cook cook = new Cook();

        Order order = new Order();
        order.setCook(cook);
        order.setDish(new Dish());
        order.refuseCooking();

        Set<Cook> set = order.getRefusalSet();

        Set<Cook> set1 = new HashSet<>(set);

        assertArrayEquals(set1.toArray(), order.getRefusalSet().toArray());
        assertNotNull(order.getCook());
        assertNotNull(order.getDish());
        assertEquals(OrderStatusEnum.REFUSAL, order.getStatus());
    }

    @Test
    void setters() {
        Dish dish = new Dish();

        Cook cook = new Cook();
        int priority = 1;

        Order order = new Order(dish, cook, priority);

        Order order1 = new Order();

        order1.setDish(dish);
        order1.setCook(cook);
        order1.setPriority(priority);
        order1.setStatus(OrderStatusEnum.COOK);

        assertEquals(order.getCook(), order1.getCook());
        assertEquals(order.getDish(), order1.getDish());
        assertEquals(OrderStatusEnum.COOK, order1.getStatus());

        assertEquals(order.getPriority(), order1.getPriority());
//        assertNull(order1.getCookingEndTime());
//        assertNull(order1.getCookingStartTime());
        assertArrayEquals(Collections.EMPTY_SET.toArray(), order1.getRefusalSet().toArray());

    }

    @Test
    void equals() {
        assertEquals(order1, order2);
    }

    @Test
    void hashCodeTest() {
        assertEquals(order1.hashCode(), order2.hashCode());
    }
}