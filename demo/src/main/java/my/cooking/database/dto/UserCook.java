package my.cooking.database.dto;

import lombok.Data;
import my.cooking.model.enums.RoleEnum;
import my.cooking.model.enums.SpecializationEnum;

import java.util.Set;

@Data
public class UserCook {

    private int id;
    private String userName;
    private String realName;
    private String password;
    private RoleEnum role;
    private int cookId;
    private Set<SpecializationEnum> specializationSet;

    public UserCook(int id, String userName, String realName, String password, RoleEnum role, Set<SpecializationEnum> specializationSet, int cookId) {
        this.id = id;
        this.userName = userName;
        this.realName = realName;
        this.password = password;
        this.role = role;
        this.specializationSet = specializationSet;
        this.cookId = cookId;
    }

    public UserCook() {

    }
}
