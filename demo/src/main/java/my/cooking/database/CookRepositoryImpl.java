package my.cooking.database;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import my.cooking.model.Cook;
import my.cooking.model.Order;
import my.cooking.model.enums.SpecializationEnum;
import my.cooking.repository.CookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.sql.ResultSet;
import java.util.List;


@Component
public class CookRepositoryImpl implements CookRepository {

    private final String createTableCook = "create table if not exists cook(" +
            "id serial primary key, " +
            "cooking boolean," +
            "waiting boolean," +
            "rating int," +
            "specializationSet varchar)";

    private final String insertCook = "INSERT INTO cook " +
            "(cooking, waiting, rating, specializationSet) " +
            "VALUES (?, ?, ?, ?) returning id";

    private final String deleteById = "delete from cook " +
            "where id = ?";

    private final String findOne = "select id, cooking, waiting, rating, specializationset " +
            "from cook " +
            "where id = ?";

    private final String updateState = "UPDATE cook " +
            "set " +
            "cooking = ?, " +
            "waiting = ? " +
            "where id = ?";

    private final String findAllReady = "select id, cooking, waiting, rating, specializationset " +
            "from cook " +
            "where cooking = false and waiting = true";

    private JdbcTemplate jdbcTemplate;

    ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    public CookRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @PostConstruct
    private void init() {
        jdbcTemplate.execute(createTableCook);
    }

    @Override
    public Cook save(Cook cook) {
        String specializationJson;
        try {
            specializationJson = objectMapper.writeValueAsString(cook.getSpecializationSet());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
        int id;
        if (cook.getId() == -1) {


            List<Integer> query = jdbcTemplate.query(insertCook, new Object[]{cook.isCooking(), cook.getRating(), specializationJson},
                    (rs, rowNum) -> rs.getInt("id"));

            id = query.get(0);
        } else {
            id = cook.getId();
            jdbcTemplate.update("UPDATE cook " +
                            "set " +
                            "cooking = ?, " +
                            "rating = ?," +
                            "specializationSet = ? " +
                            "where id = ?"
                    , cook.isCooking(), cook.getRating(), specializationJson, cook.getId());
        }

        cook.setId(id);
        return cook;
    }

    @Override
    public void delete(Cook cook) {

    }

    @Override
    public Cook findOne(int cookId) {
        List<Cook> query = jdbcTemplate.query(findOne, new Object[]{cookId},
                this::parseCook
        );
        return query.size() == 1 ? query.get(0) : null;
    }

    @Override
    public Cook findNotBusyBySpesializationNotInRefusalSetLimit1(SpecializationEnum specialization, Order order) {
        return null;
    }

    @Override
    public void updateState(Integer cookId, boolean cooking, boolean waiting) {
        jdbcTemplate.update(updateState,
                cooking, waiting, cookId);
    }

    @Override
    public List<Cook> findAllReady() {
        List<Cook> query = jdbcTemplate.query(findAllReady,
                this::parseCook
        );
        return query;
    }

    public void deleteById(int id) {
        jdbcTemplate.update(deleteById, id);
    }

    public Cook updateSpecialization(Cook cook) {
        String specializationJson;
        try {
            specializationJson = objectMapper.writeValueAsString(cook.getSpecializationSet());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
        int id;
        if (cook.getId() == -1) {


            List<Integer> query = jdbcTemplate.query(insertCook, new Object[]{cook.isCooking(), cook.isWaiting(), cook.getRating(), specializationJson},
                    (rs, rowNum) -> rs.getInt("id"));

            id = query.get(0);
        } else {
            id = cook.getId();
            jdbcTemplate.update("UPDATE cook " +
                            "set " +
                            "specializationSet = ? " +
                            "where id = ?"
                    , specializationJson, cook.getId());
        }

        cook.setId(id);
        return cook;
    }

    private Cook parseCook(ResultSet rs, int rowNumber) {
        Cook cook = new Cook();
        try {
            cook.setId(rs.getInt("id"));
            cook.setRating(rs.getInt("rating"));
            cook.setCooking(rs.getBoolean("cooking"));
            cook.setWaiting(rs.getBoolean("waiting"));
            String speciatilationset = rs.getString("specializationset");

            SpecializationEnum[] specializationEnums = objectMapper.readValue(speciatilationset, SpecializationEnum[].class);
            for (SpecializationEnum specializationEnum : specializationEnums) {
                cook.getSpecializationSet().add(specializationEnum);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return cook;
    }
}
