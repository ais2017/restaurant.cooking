package my.cooking.database;

import my.cooking.model.Dish;
import my.cooking.model.DishIngredient;
import my.cooking.model.Ingredient;
import my.cooking.model.enums.SpecializationEnum;
import my.cooking.repository.DishRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Comparator;
import java.util.List;

@Component
public class DishRepositoryImpl implements DishRepository {


    private final String createTableDish = "create table if not exists dish(" +
            "id serial primary key, " +
            "name varchar, " +
            "specialization varchar," +
            "minCookingTime int," +
            "maxCookingTime int," +
            "orderId int)";

    private final String insertDish = "insert INTO dish " +
            "(name, specialization, minCookingTime, maxCookingTime, orderId) " +
            "VALUES (?, ?, ?, ?, ?) returning id";

    private final String updateDish = "update dish " +
            "set " +
            "name = ?, " +
            "specialization = ?, " +
            "minCookingTime = ?," +
            "maxCookingTime = ?, " +
            "orderId = ? " +
            "where id = ?";

    private final String deleteDishIngredient = "delete from dishIngredient where dishId = ?";

    private final String insertDishIngredient = "insert INTO dishIngredient " +
            "(dishId) " +
            "VALUES (?) returning id";

    private final String updateDishIngredient = "update dishIngredient " +
            "set " +
            "dishId = ?" +
            "where id = ?";

    private final String createTableDishIngredient = "create table if not exists dishIngredient(" +
            "id serial primary key," +
            "dishId int references dish(id) on delete cascade on update cascade )";

    private final String createTableIngredient = "create table if not exists ingredient(" +
            "id serial primary key," +
            "name varchar," +
            "count int," +
            "dishIngredientId int references dishIngredient(id) on delete cascade on update cascade)";

    private final String insertIngredient = "insert INTO ingredient " +
            "(name, count, dishIngredientId) " +
            "VALUES (?, ?, ?) returning id";

    private final String updateIngredient = "update ingredient " +
            "set " +
            "name = ?," +
            "count = ?," +
            "dishIngredientId = ?" +
            "where id = ?";

    private final String selectAllDishWithoutOrder = "select id, name, specialization, mincookingtime, maxcookingtime " +
            "FROM dish " +
            "WHERE orderid is null";

    private final String selectDishIngredientByDishId = "select id, dishId " +
            "FROM dishIngredient " +
            "WHERE dishId = ?";

    private final String selectIngredientByDishIngredientId = "select id, name, count, dishIngredientId " +
            "FROM ingredient " +
            "WHERE dishingredientid = ?";

    private final JdbcTemplate jdbcTemplate;

    @PostConstruct
    private void init() {
        jdbcTemplate.execute(createTableDish);
        jdbcTemplate.execute(createTableDishIngredient);
        jdbcTemplate.execute(createTableIngredient);
    }

    @Autowired
    public DishRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Dish save(Dish dish) {

        jdbcTemplate.update(deleteDishIngredient, dish.getId());

        if (dish.getId() == -1) {
            List<Integer> query = jdbcTemplate.query(insertDish, new Object[]{
                            dish.getName(),
                            dish.getSpecialization().toString(),
                            dish.getMinCookingTime(),
                            dish.getMaxCookingTime(),
                            dish.getOrderId()
                    },
                    (rs, rowNum) -> rs.getInt("id"));

        dish.setId(query.get(0));
        } else {
            jdbcTemplate.update(updateDish, dish.getName(),
                    dish.getSpecialization().toString(),
                    dish.getMinCookingTime(),
                    dish.getMaxCookingTime(),
                    dish.getOrderId(),
                    dish.getId());
        }

        for (DishIngredient dishIngredient : dish.getDishIngredientSet()) {
            dishIngredient.setDishId(dish.getId());

            List<Integer> query = jdbcTemplate.query(insertDishIngredient, new Object[]{
                            dishIngredient.getDishId()
                    },
                    (rs, rowNum) -> rs.getInt("id"));
            dishIngredient.setId(query.get(0));

            for (Ingredient ingredient : dishIngredient.getSubSet()) {
                ingredient.setDishIngredientId(dishIngredient.getId());

                query = jdbcTemplate.query(insertIngredient, new Object[]{
                                ingredient.getName(),
                                ingredient.getCount(),
                                ingredient.getDishIngredientId()
                        },
                        (rs, rowNum) -> rs.getInt("id"));
                ingredient.setId(query.get(0));
            }
        }

        return dish;
    }

    @Override
    public void delete(Dish dish) {

    }

    @Override
    public List<Dish> findAll() {

        List<Dish> allDish = jdbcTemplate.query(selectAllDishWithoutOrder,
                (rs, rowNum) -> new Dish(
                        rs.getInt("id"),
                        rs.getString("name"),
                        SpecializationEnum.valueOf(rs.getString("specialization")),
                        rs.getInt("mincookingtime"),
                        rs.getInt("maxcookingtime")
                )
        );

        for (Dish dish : allDish) {
            List<DishIngredient> dishIngredients = jdbcTemplate.query(selectDishIngredientByDishId, new Object[]{dish.getId()},
                    (rs, rowNum) -> new DishIngredient(
                            rs.getInt("id"),
                            rs.getInt("dishId")
                    )
            );

            dish.getDishIngredientSet().addAll(dishIngredients);

            for (DishIngredient dishIngredient : dishIngredients) {
                List<Ingredient> ingredients = jdbcTemplate.query(selectIngredientByDishIngredientId, new Object[]{dishIngredient.getId()},
                        (rs, rowNum) -> new Ingredient(
                                rs.getInt("id"),
                                rs.getString("name"),
                                rs.getInt("count"),
                                rs.getInt("dishIngredientId")
                        )
                );

                dishIngredient.getSubSet().addAll(ingredients);
                dishIngredient.getSubSet().sort(Comparator.comparing(Ingredient::getId));
            }
        }

        allDish.sort(Comparator.comparing(Dish::getId));

        return allDish;
    }
}
