package my.cooking.database;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import my.cooking.controller.dto.response.admin.UserDtoResponse;
import my.cooking.controller.dto.response.admin.UsersDtoResponse;
import my.cooking.controller.filter.dto.RoleCookIdDto;
import my.cooking.database.dto.UserCook;
import my.cooking.model.User;
import my.cooking.model.enums.RoleEnum;
import my.cooking.model.enums.SpecializationEnum;
import my.cooking.repository.CookRepository;
import my.cooking.service.UserService;
import my.cooking.utils.SpecializationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import org.springframework.stereotype.Component;


import javax.annotation.PostConstruct;

import java.io.IOException;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class UserRepositoryImpl implements UserService {

    private final String createTableUsers = "create table if not exists users(" +
            "id serial primary key, " +
            "userName varchar," +
            "realName varchar," +
            "password varchar," +
            "token varchar," +
            "role varchar," +
            "cookId int)";

    private final String selectUserByName = "select id, userName, realName, password, role, cookId " +
            "FROM users " +
            "WHERE userName = ?";

    private final String selectUserById = "select id, userName, realName, password, role, cookId " +
            "FROM users " +
            "WHERE id = ?";

    private final String insertNewUser = "insert INTO users " +
            "(userName, realName, password,  role, cookId) " +
            "VALUES (?, ?, ?, ?, ?)";

    private final String editUser = "update users" +
            " set " +
            " userName = ?" +
            ", realName = ?" +
            ", password = ?" +
            ", token = ?" +
            ", role = ?" +
            " where id = ?";

    private final String findRoleByToken = "select role FROM users WHERE token = ?";

    private final String findRoleCookIdByToken = "select role, cookId FROM users WHERE token = ?";

    private final String findAll = "select users.id, userName, realName, password, role, specializationSet, cookId " +
            "FROM users left join cook " +
            "on users.cookId = cook.id";

    private final String editUserWithoutPassword = "update users" +
            " set " +
            " userName = ?," +
            "realName = ?," +
            "token = null, " +
            "role = ?," +
            "cookId = ?" +
            "where id = ?";

    private final String deleteById = "delete from users " +
            "where id = ?";

    private final String editUserWitPassword = "update users" +
            " set " +
            "userName = ?," +
            "realName = ?," +
            "password = ?," +
            "token = null, " +
            "role = ?," +
            "cookId = ?" +
            "where id = ?";

    private JdbcTemplate jdbcTemplate;

    private CookRepository cookRepository;

    private SpecializationMapper specializationMapper;

    ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    public UserRepositoryImpl(JdbcTemplate jdbcTemplate,
                              CookRepository cookRepository,
                              SpecializationMapper specializationMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.cookRepository = cookRepository;
        this.specializationMapper = specializationMapper;
    }

    @PostConstruct
    private void init() {
        jdbcTemplate.execute(createTableUsers);
    }

    @Override
    public User findByName(String userName) {
        List<User> query = jdbcTemplate.query(selectUserByName, new Object[]{userName},
                (rs, rowNum) -> new User(
                        rs.getInt("id"),
                        rs.getString("userName"),
                        rs.getString("realName"),
                        rs.getString("password"),
                        RoleEnum.valueOf(rs.getString("role")),
                        rs.getInt("cookId"))
        );
        return query.size() == 1 ? query.get(0) : null;
    }

    @Override
    public User addUser(User user) {

        jdbcTemplate.update(insertNewUser,
                user.getUserName(),
                user.getRealName(),
                user.getPassword(),
                user.getRole().toString(),
                user.getCookId()
        );

        return user;
    }

    @Override
    public User editUser(User user) {

        int update = jdbcTemplate.update(editUser,
                user.getUserName(),
                user.getRealName(),
                user.getPassword(),
                user.getToken(),
                user.getRole().toString(),
                user.getId()
        );

        return update == 1 ? user : null;
    }

    public String findRoleByToken(String token) {

        List<String> query = jdbcTemplate.query(findRoleByToken
                , new Object[]{token},
                (rs, rowNum) -> rs.getString("role")
        );

        return query.size() == 1 ? query.get(0) : null;
    }

    public UsersDtoResponse findAll() {
        List<UserCook> query = jdbcTemplate.query(findAll,
                (rs, rowNum) -> {
                    try {
                        Set<SpecializationEnum> specializationSet;
                        if (rs.getString("specializationSet") == null) {
                            specializationSet = new HashSet<>();
                        } else {
                            specializationSet = objectMapper.readValue(rs.getString("specializationSet"), new TypeReference<Set<SpecializationEnum>>(){});
                        }
                        return new UserCook(
                                rs.getInt("id"),
                                rs.getString("userName"),
                                rs.getString("realName"),
                                rs.getString("password"),
                                RoleEnum.valueOf(rs.getString("role")),
                                specializationSet,
                                rs.getInt("cookId"));

                    } catch (IOException e) {
                        e.printStackTrace();

                    }
                    return null;
                }
        );

        UsersDtoResponse usersDtoResponse = new UsersDtoResponse();

        for (UserCook user : query) {
            UserDtoResponse userDtoResponse = new UserDtoResponse();
            userDtoResponse.setId(user.getId());
            userDtoResponse.setRealName(user.getRealName());
            userDtoResponse.setUserName(user.getUserName());
            userDtoResponse.setRole(user.getRole().toString());
            userDtoResponse.setSpecialization(specializationMapper.map(user.getSpecializationSet()));
            usersDtoResponse.getUsers().add(userDtoResponse);
            userDtoResponse.setCookId(user.getCookId());
        }

        usersDtoResponse.getUsers().sort(Comparator.comparingInt(UserDtoResponse::getId));

        return usersDtoResponse;
    }

    public User editUserWithoutPassword(User user) {

        int update = jdbcTemplate.update(editUserWithoutPassword,
                user.getUserName(),
                user.getRealName(),
                user.getRole().toString(),
                user.getCookId(),
                user.getId()
        );

        return update == 1 ? user : null;
    }

    public User editUserWitPassword(User user) {


        int update = jdbcTemplate.update(editUserWitPassword,
                user.getUserName(),
                user.getRealName(),
                user.getPassword(),
                user.getRole().toString(),
                user.getCookId(),
                user.getId()

        );

        return update == 1 ? user : null;
    }

    public void remove(int id) {
        jdbcTemplate.update(deleteById, id);
    }

    public User findById(int id) {
        List<User> query = jdbcTemplate.query(selectUserById, new Object[]{id},
                (rs, rowNum) -> {
                    return new User(
                            rs.getInt("id"),
                            rs.getString("userName"),
                            rs.getString("realName"),
                            rs.getString("password"),
                            RoleEnum.valueOf(rs.getString("role")),
                            rs.getInt("cookId"));
                }
        );
        return query.size() == 1 ? query.get(0) : null;
    }

    public RoleCookIdDto findRoleCookIdByToken(String token) {
        List<RoleCookIdDto> query = jdbcTemplate.query(findRoleCookIdByToken
                , new Object[]{token},
                (rs, rowNum) ->
                    new RoleCookIdDto(rs.getString("role"), rs.getInt("cookId"))
        );

        return query.size() == 1 ? query.get(0) : null;
    }
}
