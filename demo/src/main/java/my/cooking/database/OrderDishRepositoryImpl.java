package my.cooking.database;

import my.cooking.model.Dish;
import my.cooking.model.DishIngredient;
import my.cooking.model.Ingredient;
import my.cooking.model.Order;
import my.cooking.model.enums.OrderStatusEnum;
import my.cooking.model.enums.SpecializationEnum;
import my.cooking.repository.OrderDishRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

@Component
public class OrderDishRepositoryImpl implements OrderDishRepository {

    private final String createTableOrderDish = "create table if not exists orderdish(" +
            "id serial primary key," +
            "cookId int," +
            "priority int," +
            "status varchar," +
            "cookingStartTime timestamp," +
            "cookingEndTime timestamp)";

    private final String findDishByCookIdAndState = "select dish.id, dish.name, priority, orderdish.cookingStartTime, dish.specialization, dish.mincookingtime, dish.maxcookingtime " +
            "FROM dish join orderdish " +
            "on dish.orderId = orderdish.id " +
            "WHERE orderid in " +
            "(select id from " +
            "orderdish " +
            "where cookid = ? and status = ?)";

    private final String findDishByCookId = "select dish.id, dish.name, priority, orderdish.cookingStartTime, dish.specialization, dish.mincookingtime, dish.maxcookingtime " +
            "FROM dish join orderdish " +
            "on dish.orderId = orderdish.id " +
            "WHERE orderid in " +
            "(select id from " +
            "orderdish " +
            "where cookid = ? and ( status = 'WAIT' or status = 'COOK'))";

    private final String selectDishIngredientByDishId = "select id, dishId " +
            "FROM dishIngredient " +
            "WHERE dishId = ?";

    private final String selectIngredientByDishIngredientId = "select id, name, count, dishIngredientId " +
            "FROM ingredient " +
            "WHERE dishingredientid = ?";

    private final String startCooking = "update orderdish " +
            "set " +
            "status = 'COOK'," +
            "cookingStartTime = now() " +
            "where cookId = ? and status = 'WAIT'";

    private final String endCooking = "update orderdish " +
            "set " +
            "status = 'DONE'," +
            "cookingEndTime = now() " +
            "where cookId = ? and status = 'COOK'";

    private final String insertOrderDish = "insert into orderdish" +
            "(priority, status) " +
            "values " +
            "(?, 'WAIT') returning id";

    private String findLowestPriorityBySpecializations =
            "select dish.id, dish.name, priority, orderdish.cookingStartTime, dish.specialization, dish.mincookingtime, dish.maxcookingtime, orderId  " +
                    "from orderdish " +
                    "       join dish " +
                    "            on dish.orderid = orderdish.id " +
                    "where status = 'WAIT' and cookId is null " +
                    "   and   specialization in (%s) " +
                    "  and priority = ( " +
                    "  select min(priority) " +
                    "  from orderdish " +
                    "         join dish " +
                    "              on dish.orderid = orderdish.id " +
                    "  where cookid is null and specialization in (%s) and status = 'WAIT')";

    private final String setCook = "update orderdish " +
            "set " +
            "cookId = ? " +
            "where id = ?";

    private final String selectCook= "update cook " +
            "set " +
            "selected = true " +
            "where id = ?";

    private final String refuseChoosing = "update orderdish " +
            "set " +
            "cookId = null " +
            "where cookId = ?";

    private final String refuseCooking = "update orderdish " +
            "set " +
            "cookId = null," +
            "cookingStartTime = null," +
            "status = 'WAIT' " +
            "where cookId = ?";

    private final JdbcTemplate jdbcTemplate;

    @PostConstruct
    private void init() {
        jdbcTemplate.execute(createTableOrderDish);
    }

    @Autowired
    public OrderDishRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Order save(Order order) {
        List<Integer> query = jdbcTemplate.query(insertOrderDish, new Object[]{order.getPriority()},
                (rs, rowNum) -> rs.getInt("id"));

        order.setId(query.get(0));
        return order;
    }

    @Override
    public Order findOne(int orderDishId) {
        return null;
    }

    @Override
    public List<Order> findOneForEachWhereStatusIsWaitingTypeSortedByPriorityAndCreatedTime() {
        return null;
    }

    @Override
    public Order findByCookId(int id) {
        return null;
    }

    private Order parseOrderDish(ResultSet rs, int rowNum) {
        Order order = new Order();

        Timestamp cookingStartTime;
        try {
            cookingStartTime = rs.getTimestamp("cookingStartTime");

            if (cookingStartTime != null) {
                order.setCookingStartTime(cookingStartTime.getTime());
                order.setPriority(rs.getInt("priority"));
            }
            order.setDish(new Dish(
                    rs.getInt("id"),
                    rs.getString("name"),
                    SpecializationEnum.valueOf(rs.getString("specialization")),
                    rs.getInt("mincookingtime"),
                    rs.getInt("maxcookingtime")));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return order;
    }

    @Override
    public Order findDishByCookIdAndStatus(Integer cookId, OrderStatusEnum status) {

        List<Order> dishes;
        if (status != null) {
            dishes = jdbcTemplate.query(findDishByCookIdAndState, new Object[]{cookId, status.toString()},
                    this::parseOrderDish

            );
        } else {
            dishes = jdbcTemplate.query(findDishByCookId, new Object[]{cookId},
                    this::parseOrderDish);
        }

        if (dishes.size() == 0) {
            return null;
        }

        Order order = dishes.get(0);

        List<DishIngredient> dishIngredients = jdbcTemplate.query(selectDishIngredientByDishId, new Object[]{order.getDish().getId()},
                (rs, rowNum) -> new DishIngredient(
                        rs.getInt("id"),
                        rs.getInt("dishId")
                )
        );

        order.getDish().getDishIngredientSet().

                addAll(dishIngredients);

        for (DishIngredient dishIngredient : dishIngredients) {
            List<Ingredient> ingredients = jdbcTemplate.query(selectIngredientByDishIngredientId, new Object[]{dishIngredient.getId()},
                    (rs, rowNum) -> new Ingredient(
                            rs.getInt("id"),
                            rs.getString("name"),
                            rs.getInt("count"),
                            rs.getInt("dishIngredientId")
                    )
            );

            dishIngredient.getSubSet().addAll(ingredients);
            dishIngredient.getSubSet().sort(Comparator.comparing(Ingredient::getId));
        }

        return order;
    }

    @Override
    public void startCooking(Integer cookId) {
        jdbcTemplate.update(startCooking, cookId);
    }

    @Override
    public void endCooking(Integer cookId) {
        jdbcTemplate.update(endCooking, cookId);
    }

    @Override
    public List<Order> findLowestPriorityBySpecializations(Set<SpecializationEnum> specializationEnumSet) {
        StringBuilder buffer = new StringBuilder();

        boolean first = true;
        for (SpecializationEnum specialization : specializationEnumSet) {
            if (first) {
                first = false;
                buffer.append(String.format("'%s'", specialization.toString()));
            } else {
                buffer.append(String.format(",'%s'", specialization.toString()));
            }
        }

        String s = buffer.toString();

        String findQuery = String.format(findLowestPriorityBySpecializations, s, s);

        return jdbcTemplate.query(findQuery,
                this::parseOrderDishId);
    }

    @Override
    public void setCook(int orderId, int cookId) {
        jdbcTemplate.update(setCook, cookId, orderId);
    }

    @Override
    public void refuseChoosing(Integer cookId) {
        jdbcTemplate.update(refuseChoosing, cookId);
    }

    @Override
    public void refuseCooking(Integer cookId) {
        jdbcTemplate.update(refuseCooking, cookId);
    }

    private Order parseOrderDishId(ResultSet rs, int rowNum) {
        Order order = new Order();

        try {
            order.setPriority(rs.getInt("priority"));
            order.setId(rs.getInt("orderId"));

            order.setDish(new Dish(
                    rs.getInt("id"),
                    rs.getString("name"),
                    SpecializationEnum.valueOf(rs.getString("specialization")),
                    rs.getInt("mincookingtime"),
                    rs.getInt("maxcookingtime")));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return order;
    }

}
