package my.cooking.repository;

import my.cooking.model.Menu;

import java.util.List;

public interface MenuRepository {

    List<Menu> getAllMenus();
    Menu getMenuById(Integer id);
    boolean addDishToMenu(Integer menuId, Integer dishId);
    boolean removeDishFromMenu(Integer menuId, Integer dishId);
    boolean deleteMenu(Integer menuId);
}
