package my.cooking.repository;

import my.cooking.model.Dish;

import java.util.List;

public interface DishRepository {

    Dish save(Dish dish);

    void delete(Dish dish);

    List<Dish> findAll();
}
