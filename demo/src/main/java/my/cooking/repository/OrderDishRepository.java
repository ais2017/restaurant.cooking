package my.cooking.repository;

import my.cooking.model.Order;
import my.cooking.model.enums.OrderStatusEnum;
import my.cooking.model.enums.SpecializationEnum;

import java.util.List;
import java.util.Set;

public interface OrderDishRepository {
    Order save(Order order);

    Order findOne(int orderDishId);
    
    List<Order> findOneForEachWhereStatusIsWaitingTypeSortedByPriorityAndCreatedTime();

    Order findByCookId(int id);

    Order findDishByCookIdAndStatus(Integer cookId, OrderStatusEnum status);

    void startCooking(Integer cookId);

    void endCooking(Integer cookId);

    List<Order> findLowestPriorityBySpecializations(Set<SpecializationEnum> specializationEnumSet);

    void setCook(int orderId, int cookId);

    void refuseChoosing(Integer cookId);

    void refuseCooking(Integer cookId);
}
