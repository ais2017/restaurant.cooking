package my.cooking.repository;

import my.cooking.model.Cook;
import my.cooking.model.Order;
import my.cooking.model.enums.SpecializationEnum;

import java.util.List;

public interface CookRepository {
    Cook save(Cook cook);

    void delete(Cook cook);

    Cook findOne(int cookId);

    Cook findNotBusyBySpesializationNotInRefusalSetLimit1(SpecializationEnum specialization, Order order);

    void updateState(Integer cookId, boolean cookId1, boolean waiting);

    List<Cook> findAllReady();
}
