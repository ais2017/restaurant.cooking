package my.cooking.repository;

import my.cooking.model.Ingredient;

public interface StoreRepository {
    boolean reserveIngredient(Ingredient ingredient);
}
