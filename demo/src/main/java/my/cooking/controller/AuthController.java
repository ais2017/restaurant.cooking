package my.cooking.controller;

import my.cooking.controller.dto.request.AuthRequestDto;
import my.cooking.controller.dto.response.auth.AuthResponseDto;
import my.cooking.database.UserRepositoryImpl;
import my.cooking.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private UserRepositoryImpl userRepository;

    @PostMapping(value = "/token")
    public ResponseEntity<?> auth(@RequestBody AuthRequestDto body)  {

        AuthResponseDto answer = new AuthResponseDto();

        User byName = userRepository.findByName(body.getUserName());

        if (byName == null || !body.getPassword().equals(byName.getPassword())) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        String token = UUID.randomUUID().toString();

        byName.setToken(token);
        userRepository.editUser(byName);

        answer.setUserName(byName.getUserName());
        answer.setRole(byName.getRole().toString());
        answer.setRealName(byName.getRealName());

        answer.setToken(token);

        return new ResponseEntity<>(answer, HttpStatus.OK);
    }
}
