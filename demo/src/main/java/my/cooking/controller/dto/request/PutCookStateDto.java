package my.cooking.controller.dto.request;

import lombok.Data;

@Data
public class PutCookStateDto {

    private boolean cooking;
    private boolean waiting;
}
