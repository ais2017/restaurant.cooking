package my.cooking.controller.dto.response.cook;

import lombok.Data;
import my.cooking.controller.dto.response.manager.DishDto;

@Data
public class DishResponseDto {

    private DishDto dish;
    private boolean cooking;
    private boolean waiting;
}
