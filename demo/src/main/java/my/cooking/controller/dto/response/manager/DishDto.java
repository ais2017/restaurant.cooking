package my.cooking.controller.dto.response.manager;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class DishDto {

    private int id;
    private String name;
    private long startCookTime;
    private long minCookTime;
    private long maxCookTime;
    private String specialization;
    private List<IngredientDto> ingredients = new ArrayList<>();
}
