package my.cooking.controller.dto.response.cook;

import lombok.Data;
import my.cooking.controller.dto.response.manager.DishDto;

@Data
public class CookStateDto {

    private boolean cooking;
    private boolean waiting;
    private DishDto dish;
}
