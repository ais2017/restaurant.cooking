package my.cooking.controller.dto.response.manager;

import lombok.Data;

@Data
public class IngredientSynonymDto {

    private int id;
    private String name;
    private int count;
}
