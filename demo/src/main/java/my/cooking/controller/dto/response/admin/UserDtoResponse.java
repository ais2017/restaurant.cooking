package my.cooking.controller.dto.response.admin;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class UserDtoResponse {

    private int id;
    private String userName;
    private String realName;
    private String role;
    private int cookId;
    private List<SpecializationDto> specialization = new ArrayList<>();
}
