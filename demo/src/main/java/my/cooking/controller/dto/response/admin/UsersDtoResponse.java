package my.cooking.controller.dto.response.admin;

import lombok.Data;
import my.cooking.model.enums.SpecializationEnum;

import java.util.ArrayList;
import java.util.List;

@Data
public class UsersDtoResponse {

    private List<UserDtoResponse> users = new ArrayList<>();
    private List<SpecializationDto> defSpecialization = new ArrayList<>();

    public UsersDtoResponse() {
        for (SpecializationEnum value : SpecializationEnum.values()) {
            defSpecialization.add(new SpecializationDto(value.name(), false));
        }
    }
}
