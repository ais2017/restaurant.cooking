package my.cooking.controller.dto.request;

import lombok.Data;

@Data
public class AuthRequestDto {

    private String userName;
    private String password;
}
