package my.cooking.controller.dto.request;

import lombok.Data;
import my.cooking.controller.dto.response.manager.DishDto;

@Data
public class DishUpdateDto {

    private DishDto dish;
}
