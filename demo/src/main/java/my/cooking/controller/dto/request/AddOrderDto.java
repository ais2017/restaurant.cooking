package my.cooking.controller.dto.request;

import lombok.Data;

@Data
public class AddOrderDto {

    private int priority;
    private int dishId;
}
