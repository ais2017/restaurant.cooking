package my.cooking.controller.dto.request;

import lombok.Data;
import my.cooking.controller.dto.response.admin.SpecializationDto;

import java.util.List;

@Data
public class PutUserRequestDto {

    private int id;
    private String userName;
    private String realName;
    private String role;
    private boolean changePassword;
    private String password;
    private int cookId;
    private List<SpecializationDto> specialization;
}
