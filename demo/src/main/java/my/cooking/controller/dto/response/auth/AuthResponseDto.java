package my.cooking.controller.dto.response.auth;

import lombok.Data;

@Data
public class AuthResponseDto {

    private String userName;
    private String realName;
    private String token;
    private String role;
}
