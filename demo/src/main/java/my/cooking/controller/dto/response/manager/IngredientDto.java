package my.cooking.controller.dto.response.manager;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class IngredientDto {

    private int id;
    private List<IngredientSynonymDto> synonyms = new ArrayList<>();
}
