package my.cooking.controller.dto.response.manager;

import lombok.Data;

import java.util.List;

@Data
public class DishResponseDto {

    private List<DishDto> dishes;
}
