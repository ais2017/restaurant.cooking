package my.cooking.controller;

import my.cooking.controller.dto.request.PutUserRequestDto;
import my.cooking.controller.dto.response.admin.SpecializationDto;
import my.cooking.controller.dto.response.admin.UsersDtoResponse;
import my.cooking.database.CookRepositoryImpl;
import my.cooking.database.UserRepositoryImpl;
import my.cooking.model.Cook;
import my.cooking.model.User;
import my.cooking.model.enums.RoleEnum;
import my.cooking.utils.SpecializationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/admin")
public class AdminController {

    private final UserRepositoryImpl userRepository;

    private final CookRepositoryImpl cookRepository;

    private final SpecializationMapper specializationMapper;

    @Autowired
    public AdminController(UserRepositoryImpl userRepository,
                           CookRepositoryImpl cookRepository,
                           SpecializationMapper specializationMapper) {
        this.userRepository = userRepository;
        this.cookRepository = cookRepository;
        this.specializationMapper = specializationMapper;
    }

    @GetMapping("/user")
    public ResponseEntity<?> getUsers() {
        UsersDtoResponse usersDtoResponse = userRepository.findAll();
        return new ResponseEntity<>(usersDtoResponse, HttpStatus.OK);
    }

    @PutMapping("/user")
    @Transactional
    public ResponseEntity<?> putUser(@RequestBody PutUserRequestDto body) {
        User newUser = new User();

        newUser.setId(body.getId());
        newUser.setRealName(body.getRealName());
        newUser.setUserName(body.getUserName());
        newUser.setRole(RoleEnum.valueOf(body.getRole()));
        newUser.setPassword(body.getPassword());
        newUser.setCookId(body.getCookId());

        updateSpecialization(newUser, body.getSpecialization());

        if (newUser.getId() == -1) {
            userRepository.addUser(newUser);
        } else {
            if (body.isChangePassword()) {
                userRepository.editUserWitPassword(newUser);
            } else {
                userRepository.editUserWithoutPassword(newUser);
            }
        }

        UsersDtoResponse usersDtoResponse = userRepository.findAll();
        return new ResponseEntity<>(usersDtoResponse, HttpStatus.OK);
    }

    @DeleteMapping("/user")
    public ResponseEntity<?> deleteUser(@PathParam("userId") int userId, @PathParam("cookId") int cookId) {

        userRepository.remove(userId);
        cookRepository.deleteById(cookId);

        UsersDtoResponse usersDtoResponse = userRepository.findAll();

        return new ResponseEntity<>(usersDtoResponse, HttpStatus.OK);
    }

    private void updateSpecialization(User newUser, List<SpecializationDto> specialization) {
        Cook cook = new Cook();
        cook.setId(newUser.getCookId());
        cook.getSpecializationSet().addAll(specializationMapper.map(specialization));
        Cook save = cookRepository.updateSpecialization(cook);
        newUser.setCookId(save.getId());
    }
}
