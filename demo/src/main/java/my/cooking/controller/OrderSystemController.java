package my.cooking.controller;

import my.cooking.controller.dto.request.AddOrderDto;
import my.cooking.model.Dish;
import my.cooking.model.Order;
import my.cooking.model.enums.OrderStatusEnum;
import my.cooking.repository.DishRepository;
import my.cooking.repository.OrderDishRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
public class OrderSystemController {

    private final DishRepository dishRepository;

    private final OrderDishRepository orderDishRepository;

    @Autowired
    public OrderSystemController(DishRepository dishRepository, OrderDishRepository orderDishRepository) {
        this.dishRepository = dishRepository;
        this.orderDishRepository = orderDishRepository;
    }

    @PostMapping("/order")
    public ResponseEntity<?> addOrder(@RequestBody AddOrderDto body) {

        for (Dish dish : dishRepository.findAll()) {
            if (dish.getId() == body.getDishId()) {

                Order order = new Order();

                order.setPriority(body.getPriority());
                order.setStatus(OrderStatusEnum.WAIT);

                order = orderDishRepository.save(order);


                dish.setId(-1);
                dish.setOrderId(order.getId());

                dishRepository.save(dish);
                return new ResponseEntity<>(order, HttpStatus.OK);
            }
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

}
