package my.cooking.controller;

import my.cooking.controller.dto.response.cook.CookStateDto;
import my.cooking.controller.dto.response.cook.DishResponseDto;
import my.cooking.controller.dto.response.manager.DishDto;
import my.cooking.model.Cook;
import my.cooking.model.Order;
import my.cooking.model.enums.OrderStatusEnum;
import my.cooking.repository.OrderDishRepository;
import my.cooking.service.CookService;
import my.cooking.utils.DishMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/cook")
public class CookController {

    private final CookService cookService;

    private final OrderDishRepository orderDishRepository;

    private final DishMapper dishMapper;

    @Autowired
    public CookController(CookService cookService, OrderDishRepository orderDishRepository, DishMapper dishMapper) {
        this.cookService = cookService;
        this.orderDishRepository = orderDishRepository;
        this.dishMapper = dishMapper;
    }

    @Transactional
    @GetMapping("/state")
    public ResponseEntity<?> getState() {
        Integer cookId = (Integer)RequestContextHolder.getRequestAttributes().getAttribute("cookId", 0);

        Cook cook = cookService.findOne(cookId);
        CookStateDto cookStateDto = new CookStateDto();

        if  (cook.isCooking()) {
            Order order = orderDishRepository.findDishByCookIdAndStatus(cookId, null);
            DishDto dishDto = dishMapper.map(order.getDish());


            cookStateDto.setDish(dishDto);
            cookStateDto.getDish().setStartCookTime(order.getCookingStartTime());
        }
        cookStateDto.setCooking(cook.isCooking());
        cookStateDto.setWaiting(cook.isWaiting());

        return new ResponseEntity<>(cookStateDto, HttpStatus.OK);
    }

    @Transactional
    @PutMapping("/waiting")
    public ResponseEntity<?> putWaiting() {
        Integer cookId = (Integer)RequestContextHolder.getRequestAttributes().getAttribute("cookId", 0);

        cookService.updateState(cookId, false, true);
        CookStateDto cookStateDto = new CookStateDto();
        cookStateDto.setCooking(false);
        cookStateDto.setWaiting(true);
        return new ResponseEntity<>(cookStateDto, HttpStatus.OK);
    }

    @Transactional
    @GetMapping("/order")
    public ResponseEntity<?> getOrder() {
        Integer cookId = (Integer)RequestContextHolder.getRequestAttributes().getAttribute("cookId", 0);

        Order order = orderDishRepository.findDishByCookIdAndStatus(cookId, OrderStatusEnum.WAIT);
        if (order != null) {
            DishDto dishDto = dishMapper.map(order.getDish());

            DishResponseDto dishResponseDto = new DishResponseDto();
            dishResponseDto.setDish(dishDto);
            dishResponseDto.setCooking(true);
            dishResponseDto.setWaiting(true);

            cookService.updateState(cookId, true, true);
            return new ResponseEntity<>(dishResponseDto, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    @Transactional
    @PutMapping("/cooking")
    public ResponseEntity<?> putCooking() {
        Integer cookId = (Integer)RequestContextHolder.getRequestAttributes().getAttribute("cookId", 0);

        cookService.startCooking(cookId);

        Order order = orderDishRepository.findDishByCookIdAndStatus(cookId, OrderStatusEnum.COOK);
        DishDto dishDto = dishMapper.map(order.getDish());


        CookStateDto cookStateDto = new CookStateDto();
        cookStateDto.setCooking(true);
        cookStateDto.setWaiting(false);
        cookStateDto.setDish(dishDto);
        cookStateDto.getDish().setStartCookTime(order.getCookingStartTime());

        return new ResponseEntity<>(cookStateDto, HttpStatus.OK);
    }

    @Transactional
    @PutMapping("/end")
    public ResponseEntity<?> putEnd() {
        Integer cookId = (Integer)RequestContextHolder.getRequestAttributes().getAttribute("cookId", 0);

        cookService.endCooking(cookId);

        CookStateDto cookStateDto = new CookStateDto();
        cookStateDto.setCooking(false);
        cookStateDto.setWaiting(false);

        return new ResponseEntity<>(cookStateDto, HttpStatus.OK);
    }

    @Transactional
    @PostMapping("stopwainting")
    public ResponseEntity<?> postStopWainting() {
        Integer cookId = (Integer)RequestContextHolder.getRequestAttributes().getAttribute("cookId", 0);

        cookService.stopWaiting(cookId);

        CookStateDto cookStateDto = new CookStateDto();
        cookStateDto.setCooking(false);
        cookStateDto.setWaiting(false);

        return new ResponseEntity<>(cookStateDto, HttpStatus.OK);
    }@Transactional

    @PostMapping("refusechoosing")
    public ResponseEntity<?> postRefuseChoosing() {
        Integer cookId = (Integer)RequestContextHolder.getRequestAttributes().getAttribute("cookId", 0);

        cookService.refuseChoosing(cookId);

        CookStateDto cookStateDto = new CookStateDto();
        cookStateDto.setCooking(false);
        cookStateDto.setWaiting(false);

        return new ResponseEntity<>(cookStateDto, HttpStatus.OK);
    }

    @PostMapping("refusecooking")
    public ResponseEntity<?> postRefuseCooking() {
        Integer cookId = (Integer)RequestContextHolder.getRequestAttributes().getAttribute("cookId", 0);

        cookService.refuseCurrentCooking(cookId);

        CookStateDto cookStateDto = new CookStateDto();
        cookStateDto.setCooking(false);
        cookStateDto.setWaiting(false);

        return new ResponseEntity<>(cookStateDto, HttpStatus.OK);
    }
 }
