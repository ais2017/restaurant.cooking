package my.cooking.controller.filter;

import my.cooking.database.UserRepositoryImpl;
import my.cooking.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AdminFilter implements Filter {

    @Autowired
    private UserRepositoryImpl userRepository;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;

        if (request.getMethod().equals("OPTIONS")) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        String requestURI = request.getRequestURI();
        if (!requestURI.startsWith("/admin/")) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
        String role = userRepository.findRoleByToken(request.getHeader("Authorization"));

        if (role == null || !role.equals("ADMIN")) {
            HttpServletResponse response = (HttpServletResponse) servletResponse;
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }
}
