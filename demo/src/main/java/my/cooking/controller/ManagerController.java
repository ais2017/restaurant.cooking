package my.cooking.controller;

import my.cooking.controller.dto.request.DishUpdateDto;
import my.cooking.controller.dto.response.manager.DishResponseDto;
import my.cooking.model.Dish;
import my.cooking.repository.DishRepository;
import my.cooking.utils.DishMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/manager")
public class ManagerController {

    private final DishRepository dishRepository;

    private final DishMapper dishMapper;

    @Autowired
    public ManagerController(DishRepository dishRepository, DishMapper dishMapper) {
        this.dishRepository = dishRepository;
        this.dishMapper = dishMapper;
    }

    @GetMapping("/dish")
    public ResponseEntity<?> getDish() {
        List<Dish> addDish = dishRepository.findAll();
        DishResponseDto dishResponseDto = dishMapper.map(addDish);
        return new ResponseEntity<>(dishResponseDto, HttpStatus.OK);
    }

    @PutMapping("/dish")
    @Transactional
    public ResponseEntity<?> putDish(@RequestBody DishUpdateDto body) {
        Dish map = dishMapper.map(body.getDish());
        dishRepository.save(map);

        List<Dish> addDish = dishRepository.findAll();

        DishResponseDto dishResponseDto = dishMapper.map(addDish);
        return new ResponseEntity<>(dishResponseDto, HttpStatus.OK);
    }
}
