package my.cooking.service;

import my.cooking.model.Dish;
import my.cooking.model.DishIngredient;
import my.cooking.model.Ingredient;
import my.cooking.repository.DishRepository;
import my.cooking.repository.StoreRepository;

public class StoreServiceImpl implements StoreService {

    private StoreRepository storeRepository;

    private DishRepository dishRepository;

    public StoreServiceImpl(StoreRepository storeRepository, DishRepository dishRepository) {
        this.storeRepository = storeRepository;
        this.dishRepository = dishRepository;
    }

    @Override
    public Dish dishIngredientReservation(Dish dish) {
        for (DishIngredient ingredient : dish.getDishIngredientSet()) {
            if (!storeRepository.reserveIngredient(ingredient.getChosenIngredient())) {
                boolean ingredientFound = false;
                for (Ingredient subIngredient : ingredient.getSubSet()) {
                    if (storeRepository.reserveIngredient(subIngredient)) {
                        ingredient.getSubSet().add(ingredient.getChosenIngredient());
                        ingredient.setChosenIngredient(subIngredient);
                        ingredient.getSubSet().remove(ingredient.getChosenIngredient());
                        ingredientFound = true;
                        break;
                    }
                }
                if (!ingredientFound) {
                    // TODO RollBack
                    return null;
                }
            }
        }

        return dishRepository.save(dish);
    }
}
