package my.cooking.service;

import my.cooking.model.Dish;

public interface DishService {

    Dish addDish(Dish dish);

    Dish editDish(Dish dish);

    void removeDish(Dish dish);
}
