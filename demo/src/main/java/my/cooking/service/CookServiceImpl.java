package my.cooking.service;

import my.cooking.model.Cook;
import my.cooking.model.Order;
import my.cooking.repository.CookRepository;
import my.cooking.repository.OrderDishRepository;

public class CookServiceImpl implements CookService {

    private CookRepository cookRepository;

    private OrderDishRepository orderDishRepository;

    public CookServiceImpl(CookRepository cookRepository, OrderDishRepository orderDishRepository) {
        this.cookRepository = cookRepository;
        this.orderDishRepository = orderDishRepository;
    }

    @Override
    public Cook editCook(Cook cook) {
        return cookRepository.save(cook);
    }

    @Override
    public Cook addCook(Cook cook) {
        return cookRepository.save(cook);
    }

    @Override
    public void removeCook(Cook cook) {
        cookRepository.delete(cook);
    }

    @Override
    public Order findNextOrder(int cookId) {
        return orderDishRepository.findByCookId(cookId);
    }

    @Override
    public Order takeOrder(int cookId, int orderDishId) {
        Order order = orderDishRepository.findOne(orderDishId);
        Cook cook = cookRepository.findOne(cookId);
        if (order == null || cook == null) {
            return null;
        }
        order.setCook(cook);
        order.startCooking();
        return orderDishRepository.save(order);
    }

    @Override
    public Order fixEndOfCooking(int cookId) {
        Order order = orderDishRepository.findByCookId(cookId);
        if (order == null || order.getCook() == null) {
            return null;
        }
        Cook cook = order.getCook();
        order.endCooking();
        cook.setCooking(false);
        return orderDishRepository.save(order);
    }

    @Override
    public boolean refuseCooking(int cookId) {
        Order order = orderDishRepository.findByCookId(cookId);
        if (order == null) {
            return false;
        }
        Cook cook = order.getCook();

        order.cookRefuse();
        order.getRefusalSet().add(cook);
        cook.setCooking(false);
        order.setCook(null);

        if (orderDishRepository.save(order) == null) {
            return false;
        }
        return cookRepository.save(cook) != null;
    }

    @Override
    public Cook findOne(Integer cookId) {
        return cookRepository.findOne(cookId);
    }

    @Override
    public void updateState(Integer cookId, boolean cooking, boolean waiting) {
        cookRepository.updateState(cookId, cooking, waiting);
    }

    @Override
    public void startCooking(Integer cookId) {
        cookRepository.updateState(cookId, true, false);
        orderDishRepository.startCooking(cookId);
    }

    @Override
    public void endCooking(Integer cookId) {
        cookRepository.updateState(cookId, false, false);
        orderDishRepository.endCooking(cookId);
    }

    @Override
    public void stopWaiting(Integer cookId) {
        cookRepository.updateState(cookId, false, false);
    }

    @Override
    public void refuseChoosing(Integer cookId) {
        cookRepository.updateState(cookId, false, false);
        orderDishRepository.refuseChoosing(cookId);
    }

    @Override
    public void refuseCurrentCooking(Integer cookId) {
        cookRepository.updateState(cookId, false, false);
        orderDishRepository.refuseCooking(cookId);
    }
}
