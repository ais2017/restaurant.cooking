package my.cooking.service;

import my.cooking.model.Menu;
import my.cooking.repository.MenuRepository;

import java.util.List;

public class MenuServiceImpl implements MenuService {

    private MenuRepository menuRepository;

    MenuServiceImpl(MenuRepository menuRepository) {
        this.menuRepository = menuRepository;
    }

    @Override
    public List<Menu> getAllMenus() {
        return menuRepository.getAllMenus();
    }

    @Override
    public Menu getMenuById(Integer id) {
        return menuRepository.getMenuById(id);
    }

    @Override
    public boolean addDishToMenu(Integer menuId, Integer dishId) {
        return menuRepository.addDishToMenu(menuId, dishId);
    }

    @Override
    public boolean removeDishFromMenu(Integer menuId, Integer dishId) {
        return menuRepository.removeDishFromMenu(menuId, dishId);
    }

    @Override
    public boolean deleteMenu(Integer menuId) {
        return menuRepository.deleteMenu(menuId);
    }
}
