package my.cooking.service;

import my.cooking.model.Dish;
import my.cooking.repository.DishRepository;

public class DishServiceImpl implements DishService {

    DishRepository dishRepository;

    DishServiceImpl(DishRepository dishRepository) {
        this.dishRepository = dishRepository;
    }

    @Override
    public Dish addDish(Dish dish) {
        return dishRepository.save(dish);
    }

    @Override
    public Dish editDish(Dish dish) {
        return dishRepository.save(dish);
    }

    @Override
    public void removeDish(Dish dish) {
        dishRepository.delete(dish);
    }
}
