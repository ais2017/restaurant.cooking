package my.cooking.service;

import my.cooking.model.User;

public interface UserService {
    User findByName(String userName);

    User addUser(User user);

    User editUser(User userAfterSave);
}
