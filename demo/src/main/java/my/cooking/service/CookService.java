package my.cooking.service;

import my.cooking.model.Cook;
import my.cooking.model.Order;

public interface CookService {

    Cook editCook(Cook cook);

    Cook addCook(Cook cook);

    void removeCook(Cook cook);

    Order findNextOrder(int cookId);

    Order takeOrder(int cookId, int orderDishId);

    Order fixEndOfCooking(int id);

    boolean refuseCooking(int id);

    Cook findOne(Integer cookId);

    void updateState(Integer cookId, boolean cooking, boolean waiting);

    void startCooking(Integer cookId);

    void endCooking(Integer cookId);

    void stopWaiting(Integer cookId);

    void refuseChoosing(Integer cookId);

    void refuseCurrentCooking(Integer cookId);
}
