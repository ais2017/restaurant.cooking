package my.cooking.service;

import my.cooking.model.Cook;
import my.cooking.model.Order;
import my.cooking.repository.CookRepository;
import my.cooking.repository.OrderDishRepository;

public class ExternalSystemServiceImpl implements ExternalSystemService {

    private OrderDishRepository orderDishRepository;
    private CookRepository cookRepository;

    ExternalSystemServiceImpl(OrderDishRepository orderDishRepository, CookRepository cookRepository) {
        this.orderDishRepository = orderDishRepository;
        this.cookRepository = cookRepository;
    }

    @Override
    public Order offer(Order order) {
        return orderDishRepository.save(order);
    }

    @Override
    public Cook updateRating(int cookId, int change) {
        Cook cook = cookRepository.findOne(cookId);
        if (cook == null) {
            return null;
        }
        cook.recalculateRating(change);
        return cookRepository.save(cook);
    }

    @Override
    public Order refuseCooking(int orderDishId) {
        Order order = orderDishRepository.findOne(orderDishId);
        if (order == null) {
            return null;
        }
        order.refuseCooking();
        return orderDishRepository.save(order);
    }

    @Override
    public Order changeCook(int orderDishId) {
        Order order = orderDishRepository.findOne(orderDishId);
        if (order == null) {
            return null;
        }
        order.cookRefuse();
        return orderDishRepository.save(order);
    }
}
