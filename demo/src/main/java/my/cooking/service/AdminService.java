package my.cooking.service;

import my.cooking.model.Cook;
import my.cooking.model.User;

public interface AdminService {
    User findByName(String userName);

    User addUser(User userBeforeSave);

    User addUser(User userAfterChange, Cook cook);

    User editUser(User userAfterSave);

    User editUser(User userAfterSave, Cook cook);
}
