package my.cooking.service;

import my.cooking.model.Cook;
import my.cooking.model.User;
import my.cooking.model.enums.RoleEnum;
import my.cooking.repository.CookRepository;

public class AdminServiceImpl implements AdminService {

    private UserService userService;
    private CookRepository cookRepository;

    public AdminServiceImpl(UserService userService, CookRepository cookRepository) {
        this.userService = userService;
        this.cookRepository = cookRepository;
    }

    @Override
    public User findByName(String userName) {
        return userService.findByName(userName);
    }

    @Override
    public User addUser(User user) {
        if (user.getRole() == RoleEnum.COOK) {
            return null;
        }

        return userService.addUser(user);
    }

    @Override
    public User addUser(User user, Cook cook) {
        if (user.getRole() != RoleEnum.COOK) {
            return null;
        }

        cook.setUserId(user.getId());
        cookRepository.save(cook);

        return userService.addUser(user);
    }

    @Override
    public User editUser(User user) {
        if (user.getRole() == RoleEnum.COOK) {
            return null;
        }
        return userService.editUser(user);
    }

    @Override
    public User editUser(User user, Cook cook) {
        if (user.getRole() != RoleEnum.COOK) {
            return null;
        }
        cook.setUserId(user.getId());
        cookRepository.save(cook);
        return userService.editUser(user);
    }
}
