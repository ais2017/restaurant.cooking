package my.cooking.service;

import my.cooking.model.Cook;
import my.cooking.model.Order;
import my.cooking.model.enums.OrderStatusEnum;
import my.cooking.repository.CookRepository;
import my.cooking.repository.OrderDishRepository;

import java.util.List;

public class OrderQueueServiceImpl implements OrderQueueService {

    private OrderDishRepository orderDishRepository;

    private CookRepository cookRepository;

    public OrderQueueServiceImpl(OrderDishRepository orderDishRepository, CookRepository cookRepository) {
        this.orderDishRepository = orderDishRepository;
        this.cookRepository = cookRepository;
    }

    @Override
    public boolean processNext() {

        List<Order> mostPriorityOrders = orderDishRepository.findOneForEachWhereStatusIsWaitingTypeSortedByPriorityAndCreatedTime();

        for (Order order : mostPriorityOrders) {
            Cook cook = cookRepository.findNotBusyBySpesializationNotInRefusalSetLimit1(order.getDish().getSpecialization(), order);
            if (cook != null) {
                order.setCook(cook);
                order.setStatus(OrderStatusEnum.IN_PROCESS);
                cook.setCooking(true);
                cookRepository.save(cook);
                orderDishRepository.save(order);
                return true;
            }
        }

        return false;
    }
}
