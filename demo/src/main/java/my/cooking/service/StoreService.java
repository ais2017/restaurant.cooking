package my.cooking.service;

import my.cooking.model.Dish;

public interface StoreService {
    Dish dishIngredientReservation(Dish dish);
}
