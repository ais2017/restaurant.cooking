package my.cooking.service;

import my.cooking.model.Cook;
import my.cooking.model.Order;

public interface ExternalSystemService {

    Order offer(Order order);

    Cook updateRating(int cookId, int i);

    Order refuseCooking(int orderDish);

    Order changeCook(int orderDishId);
}
