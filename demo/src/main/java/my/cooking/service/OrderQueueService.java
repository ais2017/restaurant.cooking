package my.cooking.service;

public interface OrderQueueService {

    boolean processNext();
}
