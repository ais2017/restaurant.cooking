package my.cooking.config;

import my.cooking.repository.CookRepository;
import my.cooking.repository.OrderDishRepository;
import my.cooking.service.CookService;
import my.cooking.service.CookServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {

    @Bean
    public CookService cookService(CookRepository cookRepository, OrderDishRepository orderDishRepository) {
        return new CookServiceImpl(cookRepository, orderDishRepository);
    }
}
