package my.cooking.model;

import java.util.*;

public class DishIngredient {

    private Integer id;

    private Ingredient chosenIngredient;

    private List<Ingredient> subSet = new ArrayList<>();

    private Integer dishId;

    public DishIngredient() {
        subSet = new ArrayList<>();
    }

    public DishIngredient(Ingredient chosenIngredient) {
        this.chosenIngredient = chosenIngredient;
        this.subSet = new ArrayList<>();
    }

    public DishIngredient(Ingredient chosenIngredient, List<Ingredient> subSet) {
        this.chosenIngredient = chosenIngredient;
        this.subSet = subSet;
    }

    public DishIngredient(int id, int dishId) {
        this.id = id;
        this.dishId = dishId;
    }

    public List<Ingredient> getSubSet() {
        return subSet;
    }

    public boolean check() {
        return chosenIngredient != null;
    }

    public Ingredient getChosenIngredient() {
        return chosenIngredient;
    }

    public void setChosenIngredient(Ingredient chosenIngredient) {
        this.chosenIngredient = chosenIngredient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DishIngredient)) return false;
        DishIngredient that = (DishIngredient) o;
        return Objects.equals(chosenIngredient, that.chosenIngredient) &&
                Objects.equals(subSet, that.subSet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(chosenIngredient, subSet);
    }

    public Integer getId() {
        return id;
    }

    public Integer getDishId() {
        return dishId;
    }

    public void setDishId(Integer dishId) {
        this.dishId = dishId;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
