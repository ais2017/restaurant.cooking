package my.cooking.model;

import my.cooking.model.enums.SpecializationEnum;

import java.util.*;

public class Dish {

    private Integer id;

    private String name;
    private Collection<DishIngredient> dishIngredientSet = new ArrayList<>();
    private SpecializationEnum specialization;
    private double minCookingTime;
    private double avgCookingTime;
    private double maxCookingTime;
    private Integer orderId;
    private List<Menu> menus = new ArrayList<>();

    public Dish() {
        dishIngredientSet = new HashSet<>();
    }

    public Dish(String name, SpecializationEnum specialization, double minCookingTime, double maxCookingTime) {
        this.name = name;
        this.specialization = specialization;
        this.minCookingTime = minCookingTime;
        this.maxCookingTime = maxCookingTime;
        this.avgCookingTime = (minCookingTime + maxCookingTime) / (double) 2;
        dishIngredientSet = new HashSet<>();
    }

    public Dish(String name, SpecializationEnum specialization, double minCookingTime,
                double maxCookingTime, Set<DishIngredient> dishIngredientSet) {
        this.name = name;
        this.specialization = specialization;
        this.minCookingTime = minCookingTime;
        this.avgCookingTime = (minCookingTime + maxCookingTime) / (double) 2;
        this.maxCookingTime = maxCookingTime;
        this.dishIngredientSet = dishIngredientSet;
    }

    public Dish(int id, String name, SpecializationEnum specialization, double minCookingTime, double maxCookingTime) {
        this.id = id;
        this.name = name;
        this.specialization = specialization;
        this.minCookingTime = minCookingTime;
        this.maxCookingTime = maxCookingTime;

    }

    public boolean checkIngredients() {
        return dishIngredientSet.stream().allMatch(DishIngredient::check);
    }

    public Collection<DishIngredient> getDishIngredientSet() {
        return dishIngredientSet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SpecializationEnum getSpecialization() {
        return specialization;
    }

    public void setSpecialization(SpecializationEnum specialization) {
        this.specialization = specialization;
    }

    public double getMinCookingTime() {
        return minCookingTime;
    }

    public void setMinCookingTime(double minCookingTime) {
        this.minCookingTime = minCookingTime;
        this.avgCookingTime = minCookingTime + maxCookingTime / (double) 2;
    }

    public double getAvgCookingTime() {
        return avgCookingTime;
    }

    public double getMaxCookingTime() {
        return maxCookingTime;
    }

    public void setMaxCookingTime(double maxCookingTime) {
        this.maxCookingTime = maxCookingTime;
        this.avgCookingTime = minCookingTime + maxCookingTime / (double) 2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Dish)) return false;
        Dish dish = (Dish) o;
        return Double.compare(dish.minCookingTime, minCookingTime) == 0 &&
                Double.compare(dish.avgCookingTime, avgCookingTime) == 0 &&
                Double.compare(dish.maxCookingTime, maxCookingTime) == 0 &&
                Objects.equals(name, dish.name) &&
                Arrays.equals(dishIngredientSet.toArray(), dish.dishIngredientSet.toArray()) &&
//                Objects.equals(dishIngredientSet, dish.dishIngredientSet) &&
                specialization == dish.specialization;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, dishIngredientSet, specialization, minCookingTime, avgCookingTime, maxCookingTime);
    }

    public Integer getId() {
        return id;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Menu> getMenus() {
        return menus;
    }

    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }
}
