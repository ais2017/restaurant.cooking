package my.cooking.model;

import java.util.Objects;

public class Ingredient {

    private Integer id;

    private String name;
    private int count;

    private int dishIngredientId;

    public Ingredient() {
    }

    public Ingredient(String name, int count) {
        this.name = name;
        this.count = count;
    }

    public Ingredient(int id, String name, int count, int dishIngredientId) {
        this.id = id;
        this.name = name;
        this.count = count;
        this.dishIngredientId = dishIngredientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ingredient that = (Ingredient) o;
        return count == that.count &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, count);
    }

    public Integer getId() {
        return id;
    }

    public int getDishIngredientId() {
        return dishIngredientId;
    }

    public void setDishIngredientId(int dishIngredientId) {
        this.dishIngredientId = dishIngredientId;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
