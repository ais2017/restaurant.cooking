package my.cooking.model.enums;

public enum OrderStatusEnum {

    WAIT,
    COOK,
    DONE,
    REFUSAL,
    IN_PROCESS
}
