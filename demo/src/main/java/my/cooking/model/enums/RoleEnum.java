package my.cooking.model.enums;

public enum RoleEnum {

    COOK,
    ADMIN,
    MANAGER
}
