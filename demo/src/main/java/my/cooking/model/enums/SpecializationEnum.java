package my.cooking.model.enums;

public enum SpecializationEnum {

    MEAT,
    FISH,
    DESSERT
}
