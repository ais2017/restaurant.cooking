package my.cooking.model;

import my.cooking.model.enums.OrderStatusEnum;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Order {

    private int id;

    private Dish dish;
    private Cook cook;
    private OrderStatusEnum status;
    private int priority;
    private Set<Cook> refusalSet;
    private long createTime;
    private long cookingStartTime;
    private long cookingEndTime;

    public void setRefusalSet(Set<Cook> refusalSet) {
        this.refusalSet = refusalSet;
    }

    public void setCookingStartTime(long cookingStartTime) {
        this.cookingStartTime = cookingStartTime;
    }

    public void setCookingEndTime(long cookingEndTime) {
        this.cookingEndTime = cookingEndTime;
    }

    public Order() {
        refusalSet = new HashSet<>();
        status = OrderStatusEnum.WAIT;
    }

    public Order(Dish dish, Cook cook, int priority) {
        this.dish = dish;
        this.cook = cook;
        this.priority = priority;
        refusalSet = new HashSet<>();
        status = OrderStatusEnum.WAIT;
    }

    public void startCooking() {
        status = OrderStatusEnum.COOK;
        cookingStartTime = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
    }

    public void endCooking() {
        status = OrderStatusEnum.DONE;
        cookingEndTime = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
    }

    public void cookRefuse() {
        status = OrderStatusEnum.WAIT;
        refusalSet.add(cook);
    }

    public void refuseCooking() {
        status = OrderStatusEnum.REFUSAL;
        cookingEndTime = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
    }

    public long getCookingStartTime() {
        return cookingStartTime;
    }

    public long getCookingEndTime() {
        return cookingEndTime;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    public Cook getCook() {
        return cook;
    }

    public void setCook(Cook cook) {
        this.cook = cook;
    }

    public OrderStatusEnum getStatus() {
        return status;
    }

    public void setStatus(OrderStatusEnum status) {
        this.status = status;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public Set<Cook> getRefusalSet() {
        return refusalSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return priority == order.priority &&
                Objects.equals(dish, order.dish) &&
                Objects.equals(cook, order.cook) &&
                status == order.status &&
                Objects.equals(refusalSet, order.refusalSet) &&
                Objects.equals(createTime, order.createTime) &&
                Objects.equals(cookingStartTime, order.cookingStartTime) &&
                Objects.equals(cookingEndTime, order.cookingEndTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dish, cook, status, priority, refusalSet, createTime, cookingStartTime, cookingEndTime);
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
