package my.cooking.model;

import my.cooking.model.enums.SpecializationEnum;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Cook {

    private int id;

    private String name;
    private boolean cooking;
    private boolean waiting;
    private boolean selected;
    private int rating;
    private Set<SpecializationEnum> specializationSet;

    private int userId;

    public Cook() {
        specializationSet = new HashSet<>();
    }

    public Cook(String name, int rating) {
        this.name = name;
        this.rating = rating;
        specializationSet = new HashSet<>();
    }

    public Cook(String name, int rating, Set<SpecializationEnum> specializationSet) {
        this.name = name;
        this.rating = rating;
        this.specializationSet = specializationSet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRating() {
        return rating;

    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Set<SpecializationEnum> getSpecializationSet() {
        return specializationSet;
    }

    public int recalculateRating(int rating) {
        return this.rating += rating;
    }

    public boolean isCooking() {
        return cooking;
    }

    public void setCooking(boolean cooking) {
        this.cooking = cooking;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cook)) return false;
        Cook cook = (Cook) o;
        return cooking == cook.cooking &&
                rating == cook.rating &&
                Objects.equals(name, cook.name) &&
                Objects.equals(specializationSet, cook.specializationSet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, cooking, rating, specializationSet);
    }

    public int getId() {
        return id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isWaiting() {
        return waiting;
    }

    public void setWaiting(boolean waiting) {
        this.waiting = waiting;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
