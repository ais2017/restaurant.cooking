package my.cooking.model;

import my.cooking.model.enums.RoleEnum;

public class User {

    private int id;

    private String userName;
    private String password;
    private String realName;
    private RoleEnum role;
    private String token;
    private Integer cookId;

    public User() {
    }

    public User(int id, String userName, String realName, String password, RoleEnum role, int cookId) {
        this.id = id;
        this.userName = userName;
        this.password = password;
        this.role = role;
        this.realName = realName;
        this.cookId = cookId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RoleEnum getRole() {
        return role;
    }

    public void setRole(RoleEnum role) {
        this.role = role;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getCookId() {
        return cookId;
    }

    public void setCookId(Integer cookId) {
        this.cookId = cookId;
    }
}
