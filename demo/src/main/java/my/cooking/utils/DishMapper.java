package my.cooking.utils;

import my.cooking.controller.dto.response.manager.DishDto;
import my.cooking.controller.dto.response.manager.DishResponseDto;
import my.cooking.controller.dto.response.manager.IngredientDto;
import my.cooking.controller.dto.response.manager.IngredientSynonymDto;
import my.cooking.model.Dish;
import my.cooking.model.DishIngredient;
import my.cooking.model.Ingredient;
import my.cooking.model.enums.SpecializationEnum;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DishMapper {

    public DishResponseDto map(List<Dish> dishes) {

        DishResponseDto dishResponseDto = new DishResponseDto();

        ArrayList<DishDto> dishDtos = new ArrayList<>();
        dishResponseDto.setDishes(dishDtos);

        for (Dish dish : dishes) {
            DishDto dishDto = new DishDto();

            dishDto.setId(dish.getId());
            dishDto.setSpecialization(dish.getSpecialization().toString());
            dishDto.setMinCookTime((int)dish.getMinCookingTime());
            dishDto.setMaxCookTime((int)dish.getMaxCookingTime());
            dishDto.setName(dish.getName());

            dishDtos.add(dishDto);

            dishDto.setIngredients(new ArrayList<>());

            for (DishIngredient dishIngredient : dish.getDishIngredientSet()) {
                IngredientDto ingredientDto = new IngredientDto();

                dishDto.getIngredients().add(ingredientDto);

                ingredientDto.setId(dishIngredient.getId());

                List<IngredientSynonymDto> ingredientSynonymDtos = new ArrayList<>();

                ingredientDto.setSynonyms(ingredientSynonymDtos);

                for (Ingredient ingredient : dishIngredient.getSubSet()) {
                    IngredientSynonymDto ingredientSynonymDto = new IngredientSynonymDto();

                    ingredientSynonymDto.setId(ingredient.getId());
                    ingredientSynonymDto.setName(ingredient.getName());
                    ingredientSynonymDto.setCount(ingredient.getCount());

                    ingredientSynonymDtos.add(ingredientSynonymDto);
                }
            }
        }
        return dishResponseDto;
    }

    public Dish map(DishDto dishDto) {

        Dish dish = new Dish();

        dish.setId(dishDto.getId());
        dish.setSpecialization(SpecializationEnum.valueOf(dishDto.getSpecialization()));
        dish.setMaxCookingTime(dishDto.getMaxCookTime());
        dish.setMinCookingTime(dishDto.getMinCookTime());
        dish.setName(dishDto.getName());

        for (IngredientDto ingredientDishDto : dishDto.getIngredients()) {
            DishIngredient dishIngredient = new DishIngredient();
            dishIngredient.setId(ingredientDishDto.getId());
            dishIngredient.setDishId(dish.getId());

            dish.getDishIngredientSet().add(dishIngredient);

            for (IngredientSynonymDto synonym : ingredientDishDto.getSynonyms()) {
                Ingredient ingredient = new Ingredient();

                ingredient.setId(synonym.getId());
                ingredient.setCount(synonym.getCount());
                ingredient.setName(synonym.getName());
                ingredient.setDishIngredientId(dishIngredient.getId());

                dishIngredient.getSubSet().add(ingredient);
            }
        }

        return dish;
    }

    public DishDto map(Dish dish) {
        DishDto dishDto = new DishDto();

        dishDto.setId(dish.getId());
        dishDto.setSpecialization(dish.getSpecialization().toString());
        dishDto.setMinCookTime((int)dish.getMinCookingTime());
        dishDto.setMaxCookTime((int)dish.getMaxCookingTime());
        dishDto.setName(dish.getName());


        dishDto.setIngredients(new ArrayList<>());

        for (DishIngredient dishIngredient : dish.getDishIngredientSet()) {
            IngredientDto ingredientDto = new IngredientDto();

            dishDto.getIngredients().add(ingredientDto);

            ingredientDto.setId(dishIngredient.getId());

            List<IngredientSynonymDto> ingredientSynonymDtos = new ArrayList<>();

            ingredientDto.setSynonyms(ingredientSynonymDtos);

            for (Ingredient ingredient : dishIngredient.getSubSet()) {
                IngredientSynonymDto ingredientSynonymDto = new IngredientSynonymDto();

                ingredientSynonymDto.setId(ingredient.getId());
                ingredientSynonymDto.setName(ingredient.getName());
                ingredientSynonymDto.setCount(ingredient.getCount());

                ingredientSynonymDtos.add(ingredientSynonymDto);
            }
        }

        return dishDto;
    }
}
