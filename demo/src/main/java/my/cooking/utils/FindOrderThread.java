package my.cooking.utils;


import my.cooking.model.Cook;
import my.cooking.model.Order;
import my.cooking.model.enums.SpecializationEnum;
import my.cooking.repository.CookRepository;
import my.cooking.repository.OrderDishRepository;
import my.cooking.service.CookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;

@Component
public class FindOrderThread extends Thread {

    private final CookService cookService;

    private final CookRepository cookRepository;

    private final OrderDishRepository orderDishRepository;

    @Autowired
    public FindOrderThread(CookService cookService, CookRepository cookRepository, OrderDishRepository orderDishRepository) {
        this.cookService = cookService;
        this.cookRepository = cookRepository;
        this.orderDishRepository = orderDishRepository;
    }

    @PostConstruct
    public void init() {
        this.start();
    }

    @Override
    public void run() {

        while (true) {
            try {
                sleep(1000);

                List<Cook> cooks = cookRepository.findAllReady();

                if (cooks.size() == 0) {
                    continue;
                }
                Set<SpecializationEnum> specializationEnumSet = new HashSet<>();

                HashMap<SpecializationEnum, Integer> specCountMap = new HashMap<>();

                for (Cook cook : cooks) {
                    specializationEnumSet.addAll(cook.getSpecializationSet());
                    for (SpecializationEnum specializationEnum : cook.getSpecializationSet()) {
                        if (specCountMap.containsKey(specializationEnum)) {
                            Integer integer = specCountMap.get(specializationEnum);
                            specCountMap.put(specializationEnum, integer + 1);
                        } else {
                            specCountMap.put(specializationEnum, 1);
                        }
                    }
                }

                if (specializationEnumSet.size() != 0) {

                    List<Order> orderList
                            = orderDishRepository.findLowestPriorityBySpecializations(specializationEnumSet);
                    if (orderList.size() == 0) {
                        continue;
                    }
                    HashMap<Cook, Order> resultMap = new HashMap<>();

                    cooks.sort(Comparator.comparingInt(cook -> cook.getSpecializationSet().size()));

                    for (Cook cook : cooks) {
                        if (orderList.size() == 0) {
                            break;
                        }
                        Order selectedOrder = null;

                        int minCount = Integer.MAX_VALUE;

                        for (SpecializationEnum anEnum : cook.getSpecializationSet()) {
                            if (specCountMap.get(anEnum) > 0 && specCountMap.get(anEnum) < minCount) {
                                for (Order order : orderList) {
                                    if (order.getDish().getSpecialization().equals(anEnum)) {
                                        minCount = specCountMap.get(anEnum);
                                        selectedOrder = order;
                                        break;
                                    }
                                }

                            }
                        }
                        if (selectedOrder != null) {
                            resultMap.put(cook, selectedOrder);
                            specCountMap.put(selectedOrder.getDish().getSpecialization(),
                                    specCountMap.get(selectedOrder.getDish().getSpecialization()) - 1);
                            orderList.remove(selectedOrder);
                        }
                    }

                    for (Map.Entry<Cook, Order> entry : resultMap.entrySet()) {
                        int cookId = entry.getKey().getId();
                        int orderId = entry.getValue().getId();

                        orderDishRepository.setCook(orderId, cookId);
                    }
                }
            } catch (Exception e) {
                System.out.println("EXCEPTION IN THE LOOP");
                e.printStackTrace();
            }
        }
    }
}
