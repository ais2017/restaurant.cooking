package my.cooking.utils;

import my.cooking.controller.dto.response.admin.SpecializationDto;
import my.cooking.model.enums.SpecializationEnum;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class SpecializationMapper {

    public List<SpecializationDto> map(Set<SpecializationEnum> set) {
        List<SpecializationDto> specialization = new ArrayList<>();

        for (SpecializationEnum value : SpecializationEnum.values()) {
            specialization.add(new SpecializationDto(value.toString(), false));
        }

        for (SpecializationDto dto : specialization) {
            if (set.contains(SpecializationEnum.valueOf(dto.getName()))) {
                dto.setFlag(true);
            }
        }

        return specialization;
    }

    public Set<SpecializationEnum> map(List<SpecializationDto> list) {
        Set<SpecializationEnum> specialization = new HashSet<>();

        for (SpecializationDto dto : list) {
            if (dto.isFlag()) {
                specialization.add(SpecializationEnum.valueOf(dto.getName()));
            }
        }

        return specialization;
    }
}
